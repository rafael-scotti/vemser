

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AgendaContatosTest {
    
    @Test
    public void pesquisaNumeroGetNome() {
        AgendaContatos agenda = new AgendaContatos();
        
        agenda.adicionar("rafael", "123");
        
        assertEquals("rafael", agenda.getNome("123"));
       
    }
    
}
