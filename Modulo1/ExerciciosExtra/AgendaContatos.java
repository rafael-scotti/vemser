import java.util.*;


public class AgendaContatos {
    HashMap<String, String> agenda = new HashMap<>();
        
    public void adicionar(String nome, String telefone) {
        if(agenda.get(nome) == null) {
            agenda.put(nome, telefone);
        }
    }
    
    public String getTelefone(String nome) {
        return agenda.get(nome);
    }
    
    public String getNome(String telefone) {
        
        for (HashMap.Entry<String, String> entry : agenda.entrySet()) {
            if (entry.getValue().equals(telefone)) {
                return (entry.getKey());
            }
        }
        
        return null;
    }
    
    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        
        for (HashMap.Entry<String, String> entry : agenda.entrySet()) {
            String chave = entry.getKey();
            String valor = entry.getValue();
            String contato = String.format("%s, %s%s", chave, valor, separador);
            builder.append(contato);
        }
        
        return builder.toString();
    }
    

}
    

