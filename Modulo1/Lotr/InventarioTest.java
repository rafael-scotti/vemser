import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest {
   
    @Test
    public void unir() {
        Inventario inventario1 = new Inventario();
        Item espada = new Item(1, "ESPADA");
        Item arco = new Item(1, "ARCO");
        inventario1.adicionar(espada);
        inventario1.adicionar(arco);
        
        Inventario inventario2 = new Inventario();
        Item faca = new Item(1, "FACA");
        inventario2.adicionar(faca);

        assertEquals(new ArrayList<Item>(Arrays.asList(espada, arco, faca)),
        inventario1.unir(inventario2).getItens());
    }
    
    @Test
    public void unirComElementoRepetido() {
        Inventario inventario1 = new Inventario();
        Inventario inventario2 = new Inventario();
        
        Item espada = new Item(1, "ESPADA");
        Item arco = new Item(1, "ARCO");
        Item faca = new Item(1, "FACA");
        
        inventario1.adicionar(espada);
        inventario1.adicionar(arco);
                
        inventario2.adicionar(faca);
        inventario2.adicionar(arco);

        assertEquals(new ArrayList<Item>(Arrays.asList(espada, arco, faca)),
        inventario1.unir(inventario2).getItens());
    }
       
    @Test
    public void adicionaItem() {
        Inventario inventario = new Inventario();
        Item item1 = new Item(1, "ESPADA");
        
        inventario.adicionar(item1);
        
        assertEquals(item1, inventario.obter(0));
 
    }
    
        @Test
    public void adiciona2Itens() {
        Inventario inventario = new Inventario();
        Item item1 = new Item(1, "ESPADA");
        Item item2 = new Item(1, "ARCO");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);

        assertEquals(item1, inventario.obter(0));
        assertEquals(item2, inventario.obter(1));
    }
    
    @Test
    public void existeSlot() {
        Inventario inventario = new Inventario();
        Item item1 = new Item(1, "ESPADA");
        Item item2 = new Item(1, "ARCO");
        
        inventario.adicionar(item1);
        inventario.adicionar(item2);
        
        assertEquals(true, inventario.existeSlot(0));
        assertEquals(true, inventario.existeSlot(1));
        assertEquals(false, inventario.existeSlot(2));
    }
     
    @Test
    public void obter() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "ESPADA");
        
        inventario.adicionar(espada);
        
        assertEquals(espada, inventario.obter(0));
        assertNull(inventario.obter(1));
    }
    
    @Test
    public void removerItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "ESPADA");
        Item martelo = new Item(1, "MARTELO");
       
        inventario.adicionar(espada);
        inventario.adicionar(martelo);
        
        assertEquals(espada, inventario.obter(0));        
        assertEquals(martelo, inventario.obter(1));
        
        inventario.remover(espada);

        assertEquals(martelo, inventario.obter(0));
        assertNull(inventario.obter(1));

    }
    
    @Test
    public void getDescricoes() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "ESPADA");
        Item item2 = new Item(1, "MARTELO");
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        
        assertEquals("ESPADA,MARTELO", inventario.getDescricoesItens()); 
    }
    
    @Test
    public void retornaItemComMaiorQuantidade() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "ESPADA");
        Item item2 = new Item(3, "MARTELO");
        Item item3 = new Item(2, "FACA");
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        assertEquals(item2, inventario.retornaItemComMaiorquantidade()); 
    }
    
    @Test
    public void retornaItemComMaiorQuantidadeEmInventarioSemItens() {
        Inventario inventario = new Inventario();  
        assertNull(inventario.retornaItemComMaiorquantidade()); 
    }
    
    @Test
    public void buscarItem() {
        Inventario inventario = new Inventario(); 
        Item espada = new Item(1, "ESPADA");
        Item martelo = new Item(3, "MARTELO");
        Item faca = new Item(2, "FACA");
        
        inventario.adicionar(espada);
        inventario.adicionar(martelo);
        inventario.adicionar(faca);
        
        assertEquals(martelo, inventario.buscar("MARTELO"));
        assertEquals(faca, inventario.buscar("FACA"));
        assertEquals(espada, inventario.buscar("ESPADA"));
    }
    
    @Test
    public void buscarItemInexistente() {
        Inventario inventario = new Inventario(); 
        Item espada = new Item(1, "ESPADA");
        
        inventario.adicionar(espada);
        
        assertEquals(null, inventario.buscar("MARTELO"));
    }
    
    @Test
    public void inverter() {
        Inventario inventario = new Inventario(); 
        Item espada = new Item(1, "ESPADA");
        Item martelo = new Item(3, "MARTELO");
        Item faca = new Item(2, "FACA");
        
        inventario.adicionar(espada);
        inventario.adicionar(martelo);
        inventario.adicionar(faca);
        
        Inventario inventarioInvertido = new Inventario();
        inventarioInvertido.adicionar(faca);   
        inventarioInvertido.adicionar(martelo);
        inventarioInvertido.adicionar(espada);

        assertEquals(inventarioInvertido.getItens(), inventario.inverter() );
    }
    
    @Test
    public void inverterVerificarPorDescricao() {
        Inventario inventario = new Inventario(); 
        Item espada = new Item(1, "ESPADA");
        Item martelo = new Item(3, "MARTELO");
        Item faca = new Item(2, "FACA");
        
        inventario.adicionar(espada);
        inventario.adicionar(martelo);
        inventario.adicionar(faca);

        assertEquals("FACA", inventario.inverter().get(0).getDescricao() );
        assertEquals("ESPADA", inventario.inverter().get(2).getDescricao() );
    }
    
    @Test
    public void inverterInventarioVazio() {
        Inventario inventario = new Inventario();
        assertTrue(inventario.inverter().isEmpty());
    }

    @Test
    public void inverterInventarioUmItem() {
        Inventario inventario = new Inventario();
        Item arco = new Item(1, "ARCO");
        inventario.adicionar(arco);
        
        assertEquals(arco, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    }
    
    @Test
    public void ordenarItens() {
        Inventario inventario = new Inventario();

        inventario.adicionar(new Item(4, "ARCO"));
        inventario.adicionar(new Item(3, "FLECHA"));
        inventario.adicionar(new Item(2, "ESPADA"));
        inventario.adicionar(new Item(1, "ESCUDO"));
       
        inventario.ordenarItens();

        assertEquals("ESCUDO,ESPADA,FLECHA,ARCO", inventario.getDescricoesItens());
    }
    
    @Test
    public void ordenarItensCasoMedio() {
        Inventario inventario = new Inventario();

        inventario.adicionar(new Item(3, "ARCO"));
        inventario.adicionar(new Item(8, "FLECHA"));
        inventario.adicionar(new Item(3, "ESPADA"));
        inventario.adicionar(new Item(1, "ESCUDO"));
       
        inventario.ordenarItens();

        assertEquals("ESCUDO,ARCO,ESPADA,FLECHA", inventario.getDescricoesItens());
    }
    
    
    @Test
    public void ordenar() {
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        
        inventario.ordenarItens();
        
        assertEquals("item3,item1,item4,item2", inventario.getDescricoesItens());
    }
    
    @Test
    public void ordenarAsc() {
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        
        inventario.ordenarItens(TipoOrdenacao.ASC);
        
        assertEquals("item3,item1,item4,item2", inventario.getDescricoesItens());
    }
        
    @Test
    public void ordenarDesc() {
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        
        inventario.ordenarItens(TipoOrdenacao.DESC);
      
        assertEquals("item2,item4,item1,item3", inventario.getDescricoesItens());
    }
}
