

public abstract class Personagem {
   protected String nome;
   protected Status status;
   protected Inventario inventario;
   protected double vida, qtdDanoSofrido;
   protected int experiencia, qtdExperienciaPorAtaque;
   
   {
       status = Status.RECEM_CRIADO;
       inventario = new Inventario();
       experiencia = 0;
       qtdExperienciaPorAtaque = 1;
       qtdDanoSofrido = 0;
   }
   
   public Personagem( String nome ) {
       this.nome = nome;
   }
   
   public String getNome() {
       return this.nome;
   }
    
   public void setNome( String nome ) {
       this.nome = nome;
   }
    
   public Status getStatus() {
       return this.status;
   }
   
   public Inventario getInventario() {
       return this.inventario;
   }
    
   public int getExperiencia() {
       return this.experiencia;
   }
    
   public void aumentarXp() {
       this.experiencia += qtdExperienciaPorAtaque;
   }
    
   public double getVida() {
       return this.vida;
   }
   
   public boolean podeSofrerDano() {
        return this.vida > 0;
   }
   
   public void ganharItem( Item item ) {
       this.inventario.adicionar( item );
   }
   
   public void perderItem( Item item ) {
       this.inventario.remover( item );
   }
   
   public void diminuirVida() {
        if( this.podeSofrerDano() && qtdDanoSofrido > 0.0) {               
           if( this.vida > this.qtdDanoSofrido ) {
               this.vida -= this.qtdDanoSofrido;
               this.status = Status.SOFREU_DANO;    
           } else {
               this.vida = 0.0;
               this.status = Status.MORTO;
           }
       }
   }
   
   //public abstract String imprimirResumo();
}
