/*Crie uma classe DwarfBarbaLonga que, ao perder vida,
tem 33,3% de chance de não perder a vida (não sofrer o dano nem ter efeito).
Utilize o dado para calcular a probabilidade.
*/
public class DwarfBarbaLonga extends Dwarf {

    private Sorteador sorteador;
    
    public DwarfBarbaLonga( String nome ) {
       super(nome);
       this.sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga(String nome, Sorteador sorteador) {
        super(nome);
        this.sorteador = sorteador;
    }
    
    @Override
    public void diminuirVida() {
        boolean devePerderVida = sorteador.sortear() > 2;
        if(devePerderVida) {
            super.diminuirVida();
        }
    }

    



}
