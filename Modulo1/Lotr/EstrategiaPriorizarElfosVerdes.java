import java.util.*;

public class EstrategiaPriorizarElfosVerdes implements EstrategiaDeAtaque{
    
    ExercitoDeElfos exercito = new ExercitoDeElfos();
    
    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos) {     
        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare( Elfo elfoAtual, Elfo proximoElfo ) {
                boolean ordemCerta = elfoAtual.getClass() == proximoElfo.getClass();
                
                if( ordemCerta ) {
                    return 0;
                }
                
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes) {
        this.removeMortos(atacantes);
        return ordenacao(exercito.getElfos());
    }
    
    public ExercitoDeElfos removeMortos(ArrayList<Elfo> elfos) {
        for(Elfo elfo : elfos) {
            if(elfo.getStatus() != Status.MORTO) {
                exercito.alistar(elfo);
            }
        }
        return exercito;
    }
}
