public class Elfo extends Personagem{
    private int indiceFlecha;
    private static int contadorDeElfos;
    {
        indiceFlecha = 0;
    }
    
    public Elfo( String nome ){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar( new Item(2, "Flecha") );
        this.inventario.adicionar( new Item(1, "Arco") );
        Elfo.contadorDeElfos++;
    }
       
    public static int getContadorDeElfos() { 
        return Elfo.contadorDeElfos;
    }
    
    protected void finalize() throws Throwable {
        Elfo.contadorDeElfos--;
    }
    
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }

    public int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
        
    public void atirarFlecha(Dwarf dwarf) {
        if( this.temFlecha() ){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.diminuirVida();
            this.aumentarXp();
            
            dwarf.diminuirVida();
        }
    }
    
    public boolean temFlecha() {
         return this.getQtdFlecha() > 0;
    }
   
}
