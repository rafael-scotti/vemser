

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    @Test
    public void nasceComArcoEFlecha() {
        ElfoVerde elfoVerde = new ElfoVerde("elfo verde");
        assertEquals("Flecha", elfoVerde.getInventario().getItens().get(0).getDescricao());
        assertEquals("Arco", elfoVerde.getInventario().getItens().get(1).getDescricao());
    }
    
    @Test
    public void ganhaExperienciaEmDobroAoAtirarFlechaPerdeFlecha(){
        ElfoVerde elfoVerde = new ElfoVerde("elfo verde");
        elfoVerde.atirarFlecha(new Dwarf("dwarf"));
        assertEquals( 2, elfoVerde.getExperiencia() );
        assertEquals( 1, elfoVerde.getQtdFlecha() );
    }
    
    
    @Test
    public void ganhaItem() {
         ElfoVerde elfoVerde = new ElfoVerde("elfo verde");
         elfoVerde.ganharItem(new Item(1, "item"));
         elfoVerde.ganharItem(new Item(1, "Flecha de Vidro"));
         assertEquals(new Item(2, "Flecha"), elfoVerde.getInventario().obter(0));
         assertNull(elfoVerde.getInventario().buscar("item"));
         assertEquals("Flecha de Vidro", elfoVerde.getInventario().buscar("Flecha de Vidro").getDescricao());
         assertEquals(new Item(1, "Flecha de Vidro"), elfoVerde.getInventario().obter(2));

        }
        
    @Test
    public void perdeItem() {
         ElfoVerde elfoVerde = new ElfoVerde("elfo verde");
         Item flechaDeVidro = new Item(1, "Flecha de Vidro");
         
         elfoVerde.ganharItem(flechaDeVidro);
         
         elfoVerde.perderItem(flechaDeVidro);
         elfoVerde.perderItem(elfoVerde.getInventario().buscar("Arco"));
         
         assertEquals("Arco", elfoVerde.getInventario().buscar("Arco").getDescricao());
         assertNull(elfoVerde.getInventario().buscar("Flecha de Vidro"));
    }

}
