import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {
    @Test
    public void pular2Limite1() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "item");
        Item item2 = new Item(1, "item2");       
        Item item3 = new Item(1, "item3");    
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        PaginadorInventario pi = new PaginadorInventario(inventario);
        
        ArrayList<Item> arrAux = new ArrayList<>();
        arrAux.add(item2);
       
        
        pi.pular(1);
        assertEquals(arrAux, pi.limitar(1));
        
    }
       
    @Test
    public void pular2Limite2() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "item");
        Item item2 = new Item(1, "item2");       
        Item item3 = new Item(1, "item3");    
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        PaginadorInventario pi = new PaginadorInventario(inventario);
        
        ArrayList<Item> arrAux = new ArrayList<>();
        arrAux.add(item2);
        arrAux.add(item3);
        
        pi.pular(1);
        assertEquals(arrAux, pi.limitar(2));
        
    }
    
    
    @Test
    public void pular1LimiteMaiorQueInventario() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "item");
        Item item2 = new Item(1, "item2");       
        Item item3 = new Item(1, "item3");    
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        PaginadorInventario pi = new PaginadorInventario(inventario);
        
        ArrayList<Item> arrAux = new ArrayList<>();
        //arrAux.add(item);
        arrAux.add(item2);
        arrAux.add(item3);
        
        pi.pular(1);
        assertEquals(arrAux, pi.limitar(4));
        
    }    
    
    
    @Test
    public void pularTodosElementos() {
        Inventario inventario = new Inventario();
        Item item = new Item(1, "item");
        Item item2 = new Item(1, "item2");       
        Item item3 = new Item(1, "item3");    
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        PaginadorInventario pi = new PaginadorInventario(inventario);
        
        ArrayList<Item> arrAux = new ArrayList<>();
        //arrAux.add(item);
        arrAux.add(item2);
        arrAux.add(item3);
        
        pi.pular(3);
        assertTrue(pi.limitar(1).isEmpty());
        
    }    
    
}
