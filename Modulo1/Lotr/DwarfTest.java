

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
    
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void diminuir10DeVida() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.diminuirVida();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    } 
    
    @Test
    public void diminuirTodaVida11Ataques() {
       Dwarf dwarf = new Dwarf("Gimli");

       for (int i = 0; i < 11; i++) {
           dwarf.diminuirVida();
       }

       assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void diminuirTodaVida12Ataques() {
       Dwarf dwarf = new Dwarf("Gimli");

       for (int i = 0; i < 12; i++) {
           dwarf.diminuirVida();
       }

       assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void mudarStatusParaMortoDepoisDePerderTodaVida() {
       Dwarf dwarf = new Dwarf("Gimli");

       for (int i = 0; i < 12; i++) {
           dwarf.diminuirVida();
       }

       assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComStatus() {
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfMudaStatusQuandoSofreDano() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.diminuirVida();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals("Escudo", dwarf.getInventario().buscar("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano() {
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.equiparEscudo();
        dwarf.diminuirVida();
        assertEquals(105.0, dwarf.getVida(), 1e-9);
    }
    
    
    
    
    
}
