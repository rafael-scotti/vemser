

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @After
    public void tearDown() {
        System.gc();
    }
    
    @Test
    public void naoCriaElfoNaoIncrementa() {
        
        assertEquals(0, Elfo.getContadorDeElfos());
    }
    
    @Test
    public void criaUmElfosContaUmaVez() {
        
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(1, Elfo.getContadorDeElfos());
    }
    @Test
    public void criaDoisElfoContaDuasVezes() {
        Elfo novoElfo = new Elfo("Legolas");
        ElfoVerde elfoVerde = new ElfoVerde("Outro Legolas");
      
        assertEquals(2, Elfo.getContadorDeElfos());
    }
    @Test
    public void atirarFlechaDwarfDiminuirFlechaAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        novoElfo.atirarFlecha(novoDwarf);
        
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(100.0, novoDwarf.getVida(), 0);
        
    }
    
    @Test
    public void atirar2FlechaDwarfDiminuir2xFlechaAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        assertEquals(90.0, novoDwarf.getVida(), 0);
        
    }
    
    @Test
    public void atirar3FlechaDwarfZerarFlechaAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        novoElfo.atirarFlecha(novoDwarf);
        
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        assertEquals(90.0, novoDwarf.getVida(), 0);
        
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
}
