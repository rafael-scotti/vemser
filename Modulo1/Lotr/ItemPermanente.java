
/**
 * Write a description of class ItemPermanente here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ItemPermanente extends Item{
    public ItemPermanente( int quantidade, String descricao ) {
        super(quantidade, descricao);
        this.setQuantidade(quantidade);
    }
    
    @Override
    public void setQuantidade( int quantidade ) {
        this.quantidade = quantidade > 1 ? quantidade : 1 ;
    }
    
}
