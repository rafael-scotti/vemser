import java.util.*;

public class ElfoVerde extends Elfo {
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano", 
            "Arco de Vidro", 
            "Flecha de Vidro"
        )
    ); 
    
    public ElfoVerde( String nome ) {
        super(nome);
        //super.ganharItem( new Item(2, "Flecha") );
        //super.ganharItem( new Item(1, "Arco") );
        this.qtdExperienciaPorAtaque = 2;
    }
    
    @Override
    public void ganharItem( Item item ) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        
        if( descricaoValida ) {
            this.inventario.adicionar( item );
        }
    }
   
    @Override
    public void perderItem( Item item ) {
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        
        if( descricaoValida ) {
            this.inventario.remover( item );
        }
    }

    
    
}
