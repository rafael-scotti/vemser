import java.util.*;

public class EstatisticasInventario {
    Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public double calculaMedia() {
        
        if(this.inventario.getItens().isEmpty()) {
            return Double.NaN;
        }
        
        double somaQuantidades = 0;
        
        for(Item item : this.inventario.getItens()) {
            somaQuantidades += item.getQuantidade();
        }
        return somaQuantidades / inventario.getItens().size();
    }
    
    public double calcularMediana() {
        Inventario inventarioAux = inventario;
        
        if(this.inventario.getItens().isEmpty()) {
            return Double.NaN;
        }
        
        inventarioAux.ordenarItens();
        
        if(inventarioAux.getTamanho() % 2 == 0){ //par
            int pos1 = (inventarioAux.getItens().size()/2) - 1;
            int pos2 = inventarioAux.getItens().size()/2; 
            
            double mediana = (inventarioAux.obter(pos1).getQuantidade() + 
                            inventarioAux.obter(pos2).getQuantidade());
            return (mediana / 2);
        }else{ //impar
            int pos = (inventarioAux.getTamanho() - 1) / 2;
            double mediana = inventarioAux.obter(pos).getQuantidade();
            return mediana;
        }
        
    }
    
    public int getQuantidadeItensAcimaDaMedia() {
        double media = this.calculaMedia();
        int qtdAcimaDaMedia = 0;
        
        for(Item item : inventario.getItens()) {
            if(item.getQuantidade() > media) qtdAcimaDaMedia++;
        }
        
        return qtdAcimaDaMedia;
    }
   
    
    
}
