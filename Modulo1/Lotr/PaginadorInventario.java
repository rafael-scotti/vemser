import java.util.*;

public class PaginadorInventario {
    private Inventario inventario;
    private int marcador;
    
    {
        marcador = 0;
    }
    
    public PaginadorInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public int getMarcador() { return this.marcador; }
    
    public void pular(int marcador) {
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int limite) {
        ArrayList<Item> arrAux = new ArrayList<>();      
        int fim = this.marcador + limite;
    
        for( int i = getMarcador(); i < fim && i < this.inventario.getItens().size(); i++ ) {
            Item item = inventario.obter(i);   
            arrAux.add( item );
        }
        return arrAux;
    }
    
    
    
    
    
    
    
  
}
