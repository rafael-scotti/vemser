

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoNoturnoTest {
    @Test
    public void ganha3ExpPerde15DeVida() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("elfo noturno");
        elfoNoturno.atirarFlecha(new Dwarf("gimli"));
        
        assertEquals(3, elfoNoturno.getExperiencia());
        assertEquals(85.0, elfoNoturno.getVida(), 1e-9); 
    }
    
    @Test
    public void atiraDuasflechas() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("elfo noturno");
        for(int i = 0; i < 2; i++) {
            elfoNoturno.atirarFlecha(new Dwarf("gimli"));
        }
        
        assertEquals(70.0, elfoNoturno.getVida(), 1e-9); 
    }
    
    @Test
    public void atiraFlechaEMorre() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("elfo noturno");
        elfoNoturno.getFlecha().setQuantidade(1000);
        for(int i = 0 ; i < 7; i++) {
            elfoNoturno.atirarFlecha(new Dwarf("dwarf"));
        }
        
        assertEquals(0.0, elfoNoturno.getVida(), 1e-9);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
        

    }
    
}
