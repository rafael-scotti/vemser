

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ExercitoDeElfosTest {
    @Test 
    public void alistaUmElfo() {
        ExercitoDeElfos elfos = new ExercitoDeElfos();
        elfos.alistar(new ElfoVerde("elfo verde"));
        assertEquals(1, elfos.getElfos().size());
    }
    
    @Test 
    public void alistaTresElfos() {
        ExercitoDeElfos elfos = new ExercitoDeElfos();
        elfos.alistar(new ElfoVerde("elfo verde1"));
        elfos.alistar(new ElfoVerde("elfo verde2"));
        elfos.alistar(new ElfoVerde("elfo verde3"));
        assertEquals(3, elfos.getElfos().size());
    }
    
    @Test 
    public void alistaTresElfosRecemCriadosEBuscaPorStatus() {
        ExercitoDeElfos elfos = new ExercitoDeElfos();
        elfos.alistar(new ElfoVerde("elfo verde1"));
        elfos.alistar(new ElfoVerde("elfo verde2"));
        elfos.alistar(new ElfoVerde("elfo verde3"));
        assertEquals(3, elfos.buscarPorStatus(Status.RECEM_CRIADO).size());
    }
    
    @Test 
    public void alistaTresElfosEBuscaPorStatus() {
        ExercitoDeElfos elfos = new ExercitoDeElfos();
        elfos.alistar(new ElfoVerde("elfo verde1"));
        
        ElfoVerde elfoVerde = new ElfoVerde("elfo verde2");
        elfoVerde.status = Status.MORTO;
        elfos.alistar(elfoVerde);
        
        elfos.alistar(new ElfoVerde("elfo verde3"));
        assertEquals(2, elfos.buscarPorStatus(Status.RECEM_CRIADO).size());
        assertEquals(1, elfos.buscarPorStatus(Status.MORTO).size());
    }
    
}
