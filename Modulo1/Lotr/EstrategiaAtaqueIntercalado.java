
import java.util.*;
/**
 * Escreva a descrição da classe EstrategiaAtaqueIntercalado aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class EstrategiaAtaqueIntercalado implements EstrategiaDeAtaque {
    ExercitoDeElfos exercito = new ExercitoDeElfos();
    private int contVerde = 0, contNoturno = 0, contador = 0;
    
   
    
    public ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos) {
        contaElfos(elfos);
        
        for(int i = 0; i < elfos.size()-1; i++) {
            
            boolean elfosIguais = elfos.get(i).getClass() == elfos.get(i+1).getClass();
            
            if( elfosIguais ) {
                Elfo elfoNecessario = elfos.get(i) instanceof ElfoVerde ? 
                    new ElfoNoturno("") : new ElfoVerde("");
       
                for(int j = i; j <= elfos.size()-1; j++) {
                    if(elfos.get(j).getClass() == elfoNecessario.getClass()){
                        elfos.add(i+1, elfos.remove(j));
                        break;
                    }
                } 
            }
        }
        return elfos;
    }
   
    public boolean possuiElfo(List<Elfo> lista, Elfo elfo, int posicaoInicial) {
        if(lista.subList(posicaoInicial, lista.size()).contains(elfo.getClass())){
            return true;
        }
        return false;
    }
    
    public void trocaPosicoes(List<Elfo> lista, int p1, int p2) {
        Elfo elfo = lista.get(p1);
        lista.set(p1, lista.get(p2));
        lista.set(p2, elfo);
    }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){
        for(Elfo elfo : this.getVivos(atacantes)) {
            exercito.alistar(elfo);
        }
        
        this.equilibraArray(exercito.getElfos());
        return ordenacao(exercito.getElfos());
       
    }
    
    public ArrayList<Elfo> getVivos(ArrayList<Elfo> elfos){
        ArrayList<Elfo> arr = new ArrayList<>();
        for(Elfo elfo : elfos) {
            if(elfo.getStatus() != Status.MORTO) {
                arr.add(elfo);
            }
        }
        return (arr);
    }
    
    public void equilibraArray(ArrayList<Elfo> elfos) {
        contaElfos(elfos);
        Elfo elfoSobrando = (this.contVerde > contNoturno) ? new ElfoVerde("") : new ElfoNoturno(""); 
        
        for(int i = elfos.size()-1; i >= contador; i--) {
            if(elfos.get(i).getClass() == elfoSobrando.getClass()) {
                elfos.remove(elfos.get(i));
            }
        }
        
    }
    
    public void contaElfos(ArrayList<Elfo> elfos) {  
        for(Elfo elfo : elfos) {
            this.contVerde += elfo.getClass() == ElfoVerde.class ? 1 : 0;
            this.contNoturno += elfo.getClass() == ElfoNoturno.class ? 1 : 0;    
        }
        contador = (this.contVerde < contNoturno) ? this.contVerde : this.contNoturno; 
    }
}
