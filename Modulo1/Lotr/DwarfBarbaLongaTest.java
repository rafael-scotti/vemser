

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfBarbaLongaTest {
    
    @Test
    public void tira2eSofreDano() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor(2);
        DwarfBarbaLonga dwarf = new DwarfBarbaLonga("dwarf", dado);
        
        dwarf.diminuirVida();
        assertEquals(110.0, dwarf.getVida(), 1e-9); 
        
    }
    
}
