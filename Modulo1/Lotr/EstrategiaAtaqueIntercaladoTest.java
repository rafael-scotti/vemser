import java.util.*;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste EstrategiaAtaqueIntercaladoTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class EstrategiaAtaqueIntercaladoTest {
    
    @Test
    public void apenasVivos() {
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
        Elfo green2 = new ElfoVerde("Verde 2");
        Elfo night4 = new ElfoNoturno("Noturno x-men 4");      
        
        night3.status = Status.MORTO;
        green1.status = Status.MORTO;
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2, night4)
        );
    
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(night1, night2, green2, night4)
        );
        
        
                
        assertEquals(elfosEsperados, estrategia.getVivos(elfosEnviados));
    
    }
    
    @Test
    public void equilibrarArray() {
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
        Elfo green2 = new ElfoVerde("Verde 2");
        Elfo night4 = new ElfoNoturno("Noturno x-men 4");      
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2, night4));
    
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, green2));
        
        estrategia.equilibraArray(elfosEnviados);            
        assertEquals(elfosEsperados, elfosEnviados);
    }
    
    @Test
    public void equilibrarArray2Elfos() {
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
      
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3));
    
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(night1, green1));
        
        estrategia.equilibraArray(elfosEnviados);            
        assertEquals(elfosEsperados, elfosEnviados);
    }
    
    
    @Test 
    public void ordenacao() {
    
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
        Elfo green2 = new ElfoVerde("Verde 2");
        Elfo green3 = new ElfoVerde("Verde 3");
        Elfo night4 = new ElfoNoturno("Noturno x-men 4");      
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2, green3, night4)
        );
    
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(night1,green1,night2, green2, night3, green3, night4)
        );
        
        ArrayList<Elfo> elfosResultado = estrategia.ordenacao(elfosEnviados);
        
        assertEquals(elfosEsperados, elfosResultado);
        assertEquals(elfosEsperados.get(2).getNome(), elfosResultado.get(2).getNome());
    }
    
    
    @Test
    public void exercitoEmbaralhadoAtaqueIntercalado() {
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
        Elfo green2 = new ElfoVerde("Verde 2");
        Elfo night4 = new ElfoNoturno("Noturno x-men 4");      
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2, night4)
        );
    
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(night1,green1,night2, green2)
        );
        
        ArrayList<Elfo> elfosResultado = estrategia.getOrdemAtaque(elfosEnviados);
        
        assertEquals(elfosEsperados, elfosResultado);
    }
}
