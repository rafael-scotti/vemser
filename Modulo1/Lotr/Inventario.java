import java.util.*;

public class Inventario {
    ArrayList<Item> itens;
        
    public Inventario() {
       itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> itensInvertido = new ArrayList<>();
        for( int i = itens.size()-1; i >= 0; i-- ) {
            itensInvertido.add( itens.get(i) );
        }
        return itensInvertido;
    }
    
    public void adicionar(Item item) { 
        this.itens.add( item );
    }
    
    public Item obter(int posicao) {
        if( existeSlot( posicao )) {
            return this.itens.get( posicao );
        }
        return null;
    }
    
    public void remover( Item item ) {
        this.itens.remove( item );
    }
    
    public boolean existeSlot(int posicao) {
        return posicao < this.itens.size();
    }

    public int getTamanho() {
        return this.itens.size();
    }
   
    public String getDescricoesItens() {
        StringBuilder desc = new StringBuilder();
        
        for( int i = 0; i < this.itens.size(); i++ ) {
            desc.append( this.itens.get(i).getDescricao() + "," );
        }
        
        return desc.length() > 0 ? 
            desc.substring( 0, (desc.length() - 1) ) :
            desc.toString()
        ;
    }
        
    public Item retornaItemComMaiorquantidade() {
        int maior = 0, indice = 0;
    
        for ( int i = 0 ; i < itens.size(); i++ ) {
            if( this.itens.get(i).getQuantidade() > maior ) {
                indice = i;
                maior = this.itens.get(i).getQuantidade();
            }
        }
        return this.itens.size() > 0 ?
            this.itens.get(indice) :
            null
        ;
    }
    
    
    public Item buscar( String descricao ) {
        for( Item item : this.itens ) {
            boolean encontrei = item.getDescricao().equals( descricao ); 
            if( encontrei ) {
                return item;
            }
        }
        return null;
    }
    
    public void ordenarItens() {
          
        for( int i = 0; i < itens.size(); i++ ) {
            for( int j = 0; j < itens.size()-1; j++ ) {
                if( this.itens.get(j).getQuantidade() > this.itens.get(j+1).getQuantidade() ){
                    Item aux = itens.get(j);
                    this.itens.set( j, itens.get(j+1) );
                    this.itens.set( j+1, aux );

                }
            }
        }
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao) {
        this.ordenarItens();
        
        if(tipoOrdenacao == TipoOrdenacao.DESC) {
            this.itens = this.inverter();
        }
    }
    
    public Inventario unir( Inventario outroInventario ) {
        Inventario novo = this;       
        for( Item item : outroInventario.getItens() ) {
            if( !novo.getItens().contains( item ) ){
                novo.adicionar( item );
            }
        }       
        return novo;
    }
    
}
