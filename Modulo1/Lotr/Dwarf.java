public class Dwarf extends Personagem{
   
   
   public Dwarf( String nome ){
       super(nome);
       this.vida = 110.0;
       this.qtdDanoSofrido = 10.0;
       this.ganharItem(new Item(1, "Escudo"));
   }
    
   public void equiparEscudo() {
        this.qtdDanoSofrido = 5.0;
   }
       
   
    
    
   
}
