import java.util.*;

public class ElfoDaLuz extends Elfo{
    
    private int contadorDeAtaques;
    private final double BUFF_DE_VIDA = 10.0;
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
            Arrays.asList(
                "Espada de Galvorn"
            )
     );

    {
        contadorDeAtaques = 0;
    }
    
    public ElfoDaLuz(String nome) {
        super(nome);
        this.qtdDanoSofrido = 21.0;
        super.ganharItem(new ItemPermanente(1, DESCRICOES_OBRIGATORIAS.get(0)));

    }
    
    public boolean devePerderVida() {
        return contadorDeAtaques % 2 == 1;
    }
    
    public void ganharVida() {
        this.vida += BUFF_DE_VIDA;
    }
    
    public void atacarComEspada ( Dwarf dwarf ) {        
        if ( this.getStatus() != Status.MORTO ) {
            dwarf.diminuirVida();
            this.aumentarXp();
            contadorDeAtaques++; 
            
            if ( devePerderVida() ) {
                this.qtdDanoSofrido = 21.0;
                this.diminuirVida();
                this.qtdDanoSofrido = 0.0;;
            } else {           
                this.ganharVida();          
            }
        }
    }
    @Override
    public void perderItem( Item item ) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if ( possoPerder ) {
            super.perderItem(item);
        } 
    }
}

