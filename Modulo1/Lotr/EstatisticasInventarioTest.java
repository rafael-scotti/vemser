

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest {
   
    @Test
    public void calculaMediaInventarioVazio() {
        Inventario inventario = new Inventario();   
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertTrue(Double.isNaN(estatisticas.calculaMedia()));
    }
    
    @Test
    public void calculaMediaApenasUmItem() {
        Inventario inventario = new Inventario();   
        inventario.adicionar(new Item(2, "item1"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
                
        assertEquals(2, estatisticas.calculaMedia(), 1e-9);
    }
    
    @Test
    public void calculaMediaComDoisItem() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(2, "item1"));
        inventario.adicionar(new Item(4, "item2"));     
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticas.calculaMedia(), 1e-9);
    }
    
    @Test
    public void calcularMedianaImpar() {
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        inventario.adicionar(new Item(1, "item5"));
        
        EstatisticasInventario ei = new EstatisticasInventario(inventario);
        
        assertEquals(1.0, ei.calcularMediana(), 1e-9);
    }
    
    @Test
    public void calcularMedianaPar() {
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        
        EstatisticasInventario ei = new EstatisticasInventario(inventario);
        
        assertEquals(1.5, ei.calcularMediana(), 1e-9);
    }
    
    @Test
    public void getQuantidadeItensAcimaDaMedia() {
        
        Inventario inventario = new Inventario();
        
        inventario.adicionar(new Item(1, "item1"));
        inventario.adicionar(new Item(3, "item2"));
        inventario.adicionar(new Item(0, "item3"));
        inventario.adicionar(new Item(2, "item4"));
        inventario.adicionar(new Item(1, "item5"));
        
        EstatisticasInventario ei = new EstatisticasInventario(inventario);

        double media = ei.calculaMedia();
        
        assertEquals(2, ei.getQuantidadeItensAcimaDaMedia());
        
    }
}
