--CREATE SEQUENCE SEQ_PAISES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--    
--CREATE TABLE PAISES(
--    ID INTEGER PRIMARY KEY,
--    NOME VARCHAR2(50) NOT NULL
--);

--CREATE SEQUENCE SEQ_ESTADOS
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE ESTADOS (
--    ID INTEGER PRIMARY KEY,
--    NOME VARCHAR2(100) NOT NULL,
--    ID_PAISES INTEGER NOT NULL,
--    FOREIGN KEY(ID_PAISES)
--        REFERENCES PAISES(ID)
--        ON DELETE CASCADE
--)

--CREATE SEQUENCE SEQ_CIDADES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE CIDADES (
--    ID INTEGER PRIMARY KEY,
--    NOME VARCHAR2(100) NOT NULL,
--    ID_ESTADOS INTEGER NOT NULL,
--    FOREIGN KEY(ID_ESTADOS)
--        REFERENCES ESTADOS(ID)
--        ON DELETE CASCADE
--)

--CREATE SEQUENCE SEQ_BANCOS
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE BANCOS (
--    CODIGO INTEGER PRIMARY KEY,
--    NOME VARCHAR2(50) NOT NULL
--)

--CREATE SEQUENCE SEQ_AGENCIAS
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE AGENCIAS (
--    CODIGO INTEGER PRIMARY KEY,
--    NOME VARCHAR2(100) NOT NULL,
--    CODIGO_BANCOS INTEGER NOT NULL,
--    ID_CIDADES INTEGER NOT NULL,
--    FOREIGN KEY(CODIGO_BANCOS)
--        REFERENCES BANCOS(CODIGO)
--        ON DELETE CASCADE,
--    FOREIGN KEY(ID_CIDADES)
--        REFERENCES CIDADES(ID)
--        ON DELETE CASCADE
--)

--CREATE SEQUENCE SEQ_TIPO_CONTA
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE TIPO_CONTA (
--    ID INTEGER PRIMARY KEY,
--    DESCRICAO VARCHAR2(20) NOT NULL
--)

--CREATE SEQUENCE SEQ_CONTAS
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE CONTAS (
--    ID INTEGER PRIMARY KEY,
--    NUMERO INTEGER NOT NULL,
--    SALDO DECIMAL DEFAULT 0 NOT NULL,
--    CODIGO_AGENCIAS INTEGER NOT NULL,
--    FOREIGN KEY(CODIGO_AGENCIAS) 
--        REFERENCES AGENCIAS(CODIGO)
--        ON DELETE CASCADE,
--    ID_TIPO_CONTA INTEGER NOT NULL,
--    FOREIGN KEY (ID_TIPO_CONTA)
--        REFERENCES TIPO_CONTA(ID)
--        ON DELETE CASCADE    
--)

--CREATE SEQUENCE SEQ_MOVIMENTACOES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE MOVIMENTACOES (
--    ID INTEGER PRIMARY KEY,
--    VALOR DECIMAL NOT NULL,
--    TIPO INTEGER CHECK(TIPO IN ('CREDITO', 'DEBITO')),
--    ID_CONTAS INTEGER NOT NULL,
--    FOREIGN KEY(ID_CONTAS) REFERENCES CONTAS(ID) ON DELETE CASCADE
--)

--CREATE SEQUENCE SEQ_CLIENTES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE CLIENTES (
--    ID INTEGER PRIMARY KEY,
--    NOME VARCHAR2(100) NOT NULL,
--    CPF CHAR(14) NOT NULL    
--)

--CREATE SEQUENCE SEQ_CIDADES_X_CLIENTES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE CIDADES_X_CLIENTES  (
--     ID_CIDADES INTEGER NOT NULL,
--     ID_CLIENTES INTEGER NOT NULL,
--     CONSTRAINT ID_CIDADE_CLIENTE PRIMARY KEY( ID_CIDADES, ID_CLIENTES ),
--     FOREIGN KEY(ID_CIDADES)
--        REFERENCES CIDADES(ID)
--        ON DELETE CASCADE,
--     FOREIGN KEY(ID_CLIENTES)
--        REFERENCES CLIENTES(ID)
--        ON DELETE CASCADE
--)

--CREATE SEQUENCE SEQ_CONTAS_X_CLIENTES
--    MINVALUE 1
--    START WITH 1
--    INCREMENT BY 1;
--
--CREATE TABLE CONTAS_X_CLIENTES  (
--     ID_CONTAS INTEGER NOT NULL,
--     ID_CLIENTES INTEGER NOT NULL,
--     CONSTRAINT ID_CONTAS_CLIENTE PRIMARY KEY( ID_CONTAS, ID_CLIENTES ),
--     FOREIGN KEY(ID_CONTAS)
--        REFERENCES CONTAS(ID)
--        ON DELETE CASCADE,
--     FOREIGN KEY(ID_CLIENTES)
--        REFERENCES CLIENTES(ID)
--        ON DELETE CASCADE
--)


