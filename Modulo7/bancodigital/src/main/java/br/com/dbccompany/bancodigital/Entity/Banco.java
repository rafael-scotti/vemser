package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name = "BANCO")
public class Banco {

    @SequenceGenerator(allocationSize = 1, initialValue = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    @Id
    @Column(name = "COD", nullable = false)
    private Integer codigo;

    @Column(name = "NOME", nullable = false)
    private String nome;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
