package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CONTAS")
public class Contas {

    @Id
    @SequenceGenerator(name = "CONTAS_SEQ", sequenceName = "CONTAS_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "CONTAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "NUMERO", nullable = false)
    private Integer numero;

    @Column(name = "SALDO", nullable = false)
    private Double saldo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_COD_AGENCIA", nullable = false)
    private Agencias codAgencia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_TIPO_CONTA", nullable = false)
    private TipoConta tipoConta;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "contas_x_clientes",
        joinColumns = { @JoinColumn( name = "id_conta" ) },
        inverseJoinColumns = { @JoinColumn( name = "id_cliente" ) })
    private List<Clientes> clientes = new ArrayList<>();

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Agencias getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(Agencias codAgencia) {
        this.codAgencia = codAgencia;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }


}
