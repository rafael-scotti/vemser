package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidades" )
public class CidadesController {
    @Autowired
    CidadesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidades> buscarTodas() {
        return service.todasCidades();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Cidades buscarId( @PathVariable Integer id ) {
        return service.buscaPorId(id);
    }

    @GetMapping( value = "/todas/{nome}" )
    @ResponseBody
    public List<Cidades> buscarNome( @PathVariable String nome ) {
        return service.buscaPorNome(nome);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Cidades adicionar( @RequestBody Cidades cidade ) {
        return service.salvar(cidade);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cidades editar( @PathVariable Integer id, @RequestBody Cidades cidade ) {
        return service.editar(cidade, id);
    }
}
