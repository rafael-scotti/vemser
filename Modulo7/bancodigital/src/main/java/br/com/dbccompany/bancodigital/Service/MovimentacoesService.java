package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Movimentacoes;
import br.com.dbccompany.bancodigital.Repository.MovimentacoesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovimentacoesService {

    @Autowired
    private MovimentacoesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Movimentacoes salvar(Movimentacoes movimentacao ) {
        return repository.save(movimentacao);
    }

    @Transactional( rollbackFor = Exception.class)
    public Movimentacoes editar( Movimentacoes movimentacao, Integer id ) {
        movimentacao.setId( id );
        return repository.save(movimentacao);
    }

    public List<Movimentacoes> todasMovimentacoes() {
        return (List<Movimentacoes>) repository.findAll();
    }

    public Movimentacoes buscaPorId( Integer id ) {
        return repository.findById(id).get();
    }

}
