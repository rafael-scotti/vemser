package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PAISES")
public class Paises {

    @Id
    @SequenceGenerator(name = "PAISES_SEQ", sequenceName = "PAISES_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "NOME", nullable = false)
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
