package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name = "MOVIMENTACOES")
public class Movimentacoes {

    @Id
    @SequenceGenerator(name = "MOVIMENTACOES_SEQ", sequenceName = "MOVIMENTACOES_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "MOVIMENTACOES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "VALOR", nullable = false)
    private Double valor;

    @Column(name = "TIPO", nullable = false)
    private Enum tipo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CONTA", nullable = false)
    private Contas conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Enum getTipo() {
        return tipo;
    }

    public void setTipo(Enum tipo) {
        this.tipo = tipo;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }
}
