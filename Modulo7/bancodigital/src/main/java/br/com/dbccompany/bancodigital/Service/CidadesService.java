package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Repository.CidadesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CidadesService {

    @Autowired
    private CidadesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Cidades salvar( Cidades cidade ) {
        return repository.save(cidade);
    }

    @Transactional( rollbackFor = Exception.class)
    public Cidades editar( Cidades cidade, Integer id ) {
        cidade.setId( id );
        return repository.save(cidade);
    }

    public List<Cidades> todasCidades() {
        return (List<Cidades>) repository.findAll();
    }

    public Cidades buscaPorId( Integer id ) {
        return repository.findById(id).get();
    }

    public List<Cidades> buscaPorNome( String nome ) {
        return repository.findAllByNome(nome);
    }
}
