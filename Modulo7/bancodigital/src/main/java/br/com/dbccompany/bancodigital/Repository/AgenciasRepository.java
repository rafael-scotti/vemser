package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Banco;
import org.springframework.data.repository.CrudRepository;

public interface AgenciasRepository  extends CrudRepository<Agencias, Integer> {
    Agencias findByNome(String nome );
}
