package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Contas;
import br.com.dbccompany.bancodigital.Service.ContasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contas" )
public class ContasController {

    @Autowired
    ContasService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Contas> buscarTodas() {
        return service.todasContas();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Contas buscarId( @PathVariable Integer id ) {
        return service.buscaPorId(id);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Contas adicionar( @RequestBody Contas conta ) {
        return service.salvar(conta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contas editar( @PathVariable Integer id, @RequestBody Contas conta ) {
        return service.editar(conta, id);
    }
}
