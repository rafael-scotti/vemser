package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Clientes> buscarTodos() {
        return service.todosClientes();
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Clientes buscarId( @PathVariable Integer id ) {
        return service.buscaPorId(id);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Clientes adicionar( @RequestBody Clientes cliente ) {
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editar( @PathVariable Integer id, @RequestBody Clientes cliente ) {
        return service.editar(cliente, id);
    }
}
