package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name = "AGENCIAS")
public class Agencias {
    @Id
    @SequenceGenerator(name = "AGENCIAS_SEQ", allocationSize = 1, sequenceName = "AGENCIAS_SEQ")
    @GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "COD", nullable = false)
    private Integer codigo;

    @Column(name = "NOME", nullable = false)
    private String nome;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_COD_BANCO", nullable = false)
    private Banco banco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CIDADE", nullable = false)
    private Cidades cidade;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }
}
