package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_CONTA")
public class TipoConta {


    @Id
    @SequenceGenerator(name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;

    @Column(name = "DESCRICAO", nullable = false)
    private String descricao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
