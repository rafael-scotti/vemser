package br.com.dbccompany.bancodigital.Entity;


import javax.persistence.*;

@Entity
@Table(name = "ESTADOS")
public class Estados {

    @Id
    @SequenceGenerator(name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "NOME", nullable = false)
    private String nome;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_PAIS")
    private Paises pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Paises getPais() {
        return pais;
    }

    public void setPais(Paises pais) {
        this.pais = pais;
    }
}
