package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Banco salvar( Banco banco ) {
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class)
    public Banco editar( Banco banco, Integer codigo ) {
        banco.setCodigo(codigo);
        return repository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public void deletar( Integer id ) {
        repository.deleteById( id );
    }

    public List<Banco> todosBancos() {
        //return repository.findAll(); // precisa declarar o metodo no BancoRepositorio
        return (List<Banco>) repository.findAll();
    }

    public Banco bancoEspecifico( Integer codigo ) {
        Optional<Banco> banco = repository.findById(codigo);
        return banco.get();
    }

    public List<Banco> todosPorNome( String nome ) {
        return repository.findAllByNome(nome);
    }
}
