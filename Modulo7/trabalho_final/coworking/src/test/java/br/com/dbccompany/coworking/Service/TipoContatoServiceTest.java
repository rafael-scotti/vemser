package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TipoContato;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TipoContatoServiceTest {

    @Autowired
    private TipoContatoService service;

    @Test
    void salvar() {
        TipoContato email = new TipoContato();
        email.setId(1);
        email.setNome("EMAIL");
        service.salvar(email);

        assertEquals("EMAIL", service.buscarRegistroPorId(1).getNome());
    }

    @Test
    void editar() {
        TipoContato email = new TipoContato();
        email.setId(2);
        email.setNome("EMAIL2");
        service.salvar(email);

        TipoContato emailModificado = new TipoContato();
        emailModificado.setNome("EMAILMODIFICADO");
        service.editar(emailModificado, 2);

        assertEquals("EMAILMODIFICADO", service.buscarRegistroPorId(2).getNome());
    }

    @Test
    void deletar() {
        TipoContato tele = new TipoContato();
        tele.setId(3);
        tele.setNome("TELE");
        service.salvar(tele);
        assertEquals("TELE", service.buscarRegistroPorId(3).getNome());
        service.deletar(3);
        assertNull(service.buscarRegistroPorId(3));
    }

    @Test
    void buscarTodosRegistros() {
        
    }

    @Test
    void buscarRegistroPorId() {
    }
}