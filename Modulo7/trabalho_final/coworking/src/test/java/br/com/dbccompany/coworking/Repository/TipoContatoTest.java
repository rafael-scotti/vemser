package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContato;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TipoContatoTest {

    @Autowired
    TipoContatoRepository repository;

    @Test
    @Transactional( rollbackFor = Exception.class)
    public void adicionarTipo() {
        TipoContato email = new TipoContato();
        TipoContato tele = new TipoContato();
        email.setId(null);
        email.setNome("Email");
        repository.save(email);
        tele.setId(null);
        tele.setNome("Telefone");
        repository.save(tele);

        assertEquals("Telefone", repository.findById(2).get().getNome());
        assertEquals("Email", repository.findById(1).get().getNome());
    }
}