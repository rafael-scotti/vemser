package br.com.dbccompany.coworking.Service;


import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Clientes salvar(Clientes cliente ) {
        return repository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar( Clientes cliente, Integer id ) {
        cliente.setId( id );
        return repository.save( cliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Clientes> buscarTodosRegistros() {
        return (List<Clientes>) repository.findAll();
    }

    public Clientes buscarRegistroPorId( Integer id ) {
        Optional<Clientes> cliente = repository.findById( id );
        return cliente.orElse(null);
    }

}
