package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "ESPACOS" )
public class Espacos {

    @SequenceGenerator( allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", unique = true, nullable = false )
    private String nome;

    @Column( name = "QTD_PESSOAS", nullable = false )
    private Integer qtdPessoas;

    @Column( name = "VALOR", nullable = false )
    private Double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco" )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "espaco")
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
