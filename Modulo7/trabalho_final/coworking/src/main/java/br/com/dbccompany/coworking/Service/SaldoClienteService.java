package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteKey;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;


    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente salvar(SaldoCliente saldoCliente ) {
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoCliente editar( SaldoCliente saldoCliente, SaldoClienteKey id ) {
        saldoCliente.setId( id );
        return repository.save( saldoCliente );
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteKey deletar( SaldoClienteKey id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return null;
        }
    }

    public List<SaldoCliente> buscarTodosRegistros() {
        return (List<SaldoCliente>) repository.findAll();
    }

    public SaldoCliente buscarRegistroPorId( SaldoClienteKey id ) {
        Optional<SaldoCliente> sc = repository.findById( id );
        return sc.orElse(null);
    }

}
