package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/clientesPacotes" )
public class ClientesPacotesController {

    @Autowired
    private ClientesPacotesService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public ClientesPacotes adicionar( @RequestBody ClientesPacotes clientesPacotes ) {
        return service.salvar(clientesPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotes editar( @RequestBody ClientesPacotes clientesPacotes, @PathVariable Integer id ) {
        return service.editar( clientesPacotes, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<ClientesPacotes> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public ClientesPacotes buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
