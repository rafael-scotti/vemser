package br.com.dbccompany.coworking.Entity;

public enum TipoContratacao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
