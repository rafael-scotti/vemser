package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table( name = "CLIENTES" )
public class Clientes {

    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "CPF", nullable = false, unique = true)
    private String cpf;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm" )
    @Column( name = "DATA_NASCIMENTO", nullable = false )
    private Date dataNascimento;

    @OneToMany( mappedBy = "cliente" )
    private List<Contato> contatos = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente" )
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente")
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
