package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pagamentos" )
public class PagamentosController {

    @Autowired
    private PagamentosService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Pagamentos adicionar(@RequestBody Pagamentos pagamento ) {
        return service.salvar(pagamento);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamentos editar( @RequestBody Pagamentos pagamento, @PathVariable Integer id ) {
        return service.editar( pagamento, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Pagamentos> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Pagamentos buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
