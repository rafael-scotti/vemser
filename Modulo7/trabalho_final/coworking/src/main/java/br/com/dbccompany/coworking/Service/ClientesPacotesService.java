package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesPacotesService {

    @Autowired
    private ClientesPacotesRepository repository;


    @Transactional( rollbackFor = Exception.class )
    public ClientesPacotes salvar( ClientesPacotes clientesPacotes ) {
        return repository.save( clientesPacotes );
    }

    @Transactional( rollbackFor = Exception.class)
    public ClientesPacotes editar( ClientesPacotes clientesPacotes, Integer id ) {
        clientesPacotes.setId( id );
        return repository.save( clientesPacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<ClientesPacotes> buscarTodosRegistros() {
        return (List<ClientesPacotes>) repository.findAll();
    }

    public ClientesPacotes buscarRegistroPorId( Integer id ) {
        Optional<ClientesPacotes> cp = repository.findById( id );
        return cp.orElse(null);
    }


}
