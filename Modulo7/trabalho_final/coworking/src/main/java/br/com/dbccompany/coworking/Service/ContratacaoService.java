package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Contratacao salvar(Contratacao contratacao ) {
        return repository.save( contratacao );
    }

    @Transactional( rollbackFor = Exception.class)
    public Contratacao editar( Contratacao contratacao, Integer id ) {
        contratacao.setId( id );
        return repository.save( contratacao );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Contratacao> buscarTodosRegistros() {
        return (List<Contratacao>) repository.findAll();
    }

    public Contratacao buscarRegistroPorId( Integer id ) {
        Optional<Contratacao> contratacao = repository.findById( id );
        return contratacao.orElse(null);
    }


}
