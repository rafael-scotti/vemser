package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/pacotes" )
public class PacotesController {

    @Autowired
    private PacotesService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Pacotes adicionar( @RequestBody Pacotes pacote ) {
        return service.salvar(pacote);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pacotes editar( @RequestBody Pacotes pacote, @PathVariable Integer id ) {
        return service.editar( pacote, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Pacotes> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Pacotes buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
