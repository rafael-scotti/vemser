package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Entity
@Table( name = "USUARIOS" )
public class Usuarios {

    @SequenceGenerator( allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
    @GeneratedValue( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "EMAIL", nullable = false, unique = true )
    private String email;

    @Column( name = "LOGIN", nullable = false, unique = true )
    private String login;

    @Size(min = 6)
    @Column( name = "SENHA", nullable = false )
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {

        try{
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(senha.getBytes());
            byte[] digest = md.digest();
            String myHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();

            this.senha = myHash;
        }catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        }

    }
}
