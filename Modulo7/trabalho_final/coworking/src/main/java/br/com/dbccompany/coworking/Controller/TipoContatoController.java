package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoContato" )
public class TipoContatoController {

    @Autowired
    TipoContatoService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public TipoContato adicionar(@RequestBody TipoContato tipoContato ) {
        return service.salvar(tipoContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editar( @RequestBody TipoContato tipoContato, @PathVariable Integer id ) {
        return service.editar( tipoContato, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<TipoContato> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public TipoContato buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
