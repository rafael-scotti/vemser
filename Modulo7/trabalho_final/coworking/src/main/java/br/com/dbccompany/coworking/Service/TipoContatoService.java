package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public TipoContato salvar( TipoContato tipoContato ) {
        return repository.save( tipoContato );
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContato editar( TipoContato tipoContato, Integer id ) {
        tipoContato.setId( id );
        return repository.save( tipoContato );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<TipoContato> buscarTodosRegistros() {
        return (List<TipoContato>) repository.findAll();
    }

    public TipoContato buscarRegistroPorId( Integer id ) {
        Optional<TipoContato> tc = repository.findById( id );
        return tc.orElse(null);
    }


}
