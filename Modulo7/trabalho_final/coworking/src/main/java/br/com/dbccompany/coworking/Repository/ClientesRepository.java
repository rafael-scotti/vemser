package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
}
