package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvar( Contato contato ) {
        return repository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class)
    public Contato editar(Contato contato, Integer id ) {
        contato.setId( id );
        return repository.save( contato );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Contato> buscarTodosRegistros() {
        return (List<Contato>) repository.findAll();
    }

    public Contato buscarRegistroPorId( Integer id ) {
        Optional<Contato> contato = repository.findById( id );
        return contato.orElse(null);
    }


}
