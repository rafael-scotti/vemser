package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Pagamentos salvar(Pagamentos pagamento ) {
        return repository.save( pagamento );
    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos editar( Pagamentos pagamento, Integer id ) {
        pagamento.setId( id );
        return repository.save( pagamento );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Pagamentos> buscarTodosRegistros() {
        return (List<Pagamentos>) repository.findAll();
    }

    public Pagamentos buscarRegistroPorId( Integer id ) {
        Optional<Pagamentos> pagamento = repository.findById( id );
        return pagamento.orElse(null);
    }

}
