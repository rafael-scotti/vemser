package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "ESPACOS_X_PACOTES" )
public class EspacosPacotes {

    @SequenceGenerator( allocationSize = 1, name = "ESPACOS_X_PACOTES_SEQ", sequenceName = "ESPACOS_X_PACOTES_SEQ")
    @GeneratedValue( generator = "ESPACOS_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @ManyToOne
    @JoinColumn( name = "FK_ID_ESPACO" )
    private Espacos espaco;

    @ManyToOne
    @JoinColumn( name = "FK_ID_PACOTE")
    private Pacotes pacote;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO" )
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    @Column( name = "PRAZO" )
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }


}
