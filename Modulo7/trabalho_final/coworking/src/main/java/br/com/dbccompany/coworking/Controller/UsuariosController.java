package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/login" )
public class UsuariosController {

    @Autowired
    private UsuariosService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Usuarios adicionar(@RequestBody Usuarios usuario ) {
        return service.salvar(usuario);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editar( @RequestBody Usuarios usuario, @PathVariable Integer id ) {
        return service.editar( usuario, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Usuarios> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Usuarios buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
