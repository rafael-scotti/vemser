package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteKey;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/saldoCliente" )
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public SaldoCliente adicionar( @RequestBody SaldoCliente saldoCliente ) {
        return service.salvar( saldoCliente );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editar( @RequestBody SaldoCliente saldoCliente, @PathVariable SaldoClienteKey id ) {
        return service.editar( saldoCliente, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<SaldoCliente> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public SaldoCliente buscarRegistroPorId( @PathVariable  SaldoClienteKey id ) {
        return service.buscarRegistroPorId( id );
    }
}
