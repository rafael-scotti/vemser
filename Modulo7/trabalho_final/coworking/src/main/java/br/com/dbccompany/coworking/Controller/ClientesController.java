package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/clientes" )
public class ClientesController {

    @Autowired
    private ClientesService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Clientes adicionar(@RequestBody Clientes cliente ) {
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editar( @RequestBody Clientes cliente, @PathVariable Integer id ) {
        return service.editar( cliente, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Clientes> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Clientes buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}