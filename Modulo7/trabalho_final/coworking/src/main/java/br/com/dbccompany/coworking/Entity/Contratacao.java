package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CONTRATACAO" )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Contratacao {

    @SequenceGenerator( allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_ESPACO", nullable = false)
    private Espacos espaco;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private Clientes cliente;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @Column( name = "DESCONTO", nullable = true )
    private Double desconto;

    @Column( name = "PRAZO", nullable = false)
    private Integer prazo;

    @OneToMany(mappedBy = "contratacao")
    private List<Pagamentos> pagamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    @JsonSerialize
    public String valorCobrado() {
        return  "R$"; //this.quantidade * this.espaco.getValor();
    }
}
