package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "CLIENTES_X_PACOTES" )
public class ClientesPacotes {

    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ")
    @GeneratedValue( generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private Clientes cliente;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_PACOTE", nullable = false )
    private Pacotes pacote;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<Pagamentos> pagamentos = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
