package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {
}
