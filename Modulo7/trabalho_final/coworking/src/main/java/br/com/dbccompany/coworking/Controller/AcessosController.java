package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/acessos" )
public class AcessosController {

    @Autowired
    private AcessosService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Acessos adicionar(@RequestBody Acessos acesso ) {
        return service.salvar(acesso);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editar( @RequestBody Acessos acesso, @PathVariable Integer id ) {
        return service.editar( acesso, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Acessos> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Acessos buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
