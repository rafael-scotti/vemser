package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Espacos salvar(Espacos espaco ) {
        return repository.save( espaco );
    }

    @Transactional( rollbackFor = Exception.class)
    public Espacos editar( Espacos espaco, Integer id ) {
        espaco.setId( id );
        return repository.save( espaco );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Espacos> buscarTodosRegistros() {
        return (List<Espacos>) repository.findAll();
    }

    public Espacos buscarRegistroPorId( Integer id ) {
        Optional<Espacos> tc = repository.findById( id );
        return tc.orElse(null);
    }

}
