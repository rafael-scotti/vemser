package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espacos" )
public class EspacosController {

    @Autowired
    private EspacosService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Espacos adicionar(@RequestBody Espacos espaco ) {
        return service.salvar(espaco);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espacos editar( @RequestBody Espacos espaco, @PathVariable Integer id ) {
        return service.editar( espaco, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Espacos> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Espacos buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
