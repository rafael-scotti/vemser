package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;

@Entity
@Table( name = "CONTATO" )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Contato {

    @SequenceGenerator( allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE )
    @Id
    @Column( name = "ID", nullable = false )
    private Integer id;

    @Column( name = "VALOR", nullable = false )
    private String valor;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_TIPO_CONTATO", nullable = false )
    private TipoContato tipoContato;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CLIENTE", nullable = false )
    private Clientes cliente;

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

}
