package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contratacao" )
public class ContratacaoController {

    @Autowired
    private ContratacaoService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Contratacao adicionar( @RequestBody Contratacao contratacao ) {
        return service.salvar(contratacao);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editar( @RequestBody Contratacao contratacao, @PathVariable Integer id ) {
        return service.editar( contratacao, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Contratacao> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Contratacao buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
