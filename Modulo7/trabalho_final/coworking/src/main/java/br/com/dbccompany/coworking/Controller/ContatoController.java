package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/contato" )
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public Contato adicionar(@RequestBody Contato contato ) {
        return service.salvar(contato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contato editar( @RequestBody Contato contato, @PathVariable Integer id ) {
        return service.editar( contato, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<Contato> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public Contato buscarRegistroPorId( @PathVariable Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
