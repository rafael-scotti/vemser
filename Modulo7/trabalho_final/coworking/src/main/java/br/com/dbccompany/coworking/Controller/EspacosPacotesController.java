package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/espacosPacotes" )
public class EspacosPacotesController {

    @Autowired
    private EspacosPacotesService service;

    @PostMapping( value = "/adicionar" )
    @ResponseBody
    public EspacosPacotes adicionar(@RequestBody EspacosPacotes espacosPacotes ) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotes editar( @RequestBody EspacosPacotes espacosPacotes, @PathVariable Integer id ) {
        return service.editar( espacosPacotes, id );
    }

    @GetMapping( value = "/buscar" )
    @ResponseBody
    public List<EspacosPacotes> buscarTodosRegistros() {
        return service.buscarTodosRegistros();
    }

    @GetMapping( value = "/buscar/{id}" )
    @ResponseBody
    public EspacosPacotes buscarRegistroPorId( @PathVariable  Integer id ) {
        return service.buscarRegistroPorId( id );
    }
}
