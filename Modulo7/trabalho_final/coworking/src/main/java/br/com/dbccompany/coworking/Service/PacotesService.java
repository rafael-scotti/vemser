package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;


    @Transactional( rollbackFor = Exception.class )
    public Pacotes salvar(Pacotes pacote ) {
        return repository.save( pacote );
    }

    @Transactional( rollbackFor = Exception.class)
    public Pacotes editar( Pacotes pacote, Integer id ) {
        pacote.setId( id );
        return repository.save( pacote );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Pacotes> buscarTodosRegistros() {
        return (List<Pacotes>) repository.findAll();
    }

    public Pacotes buscarRegistroPorId( Integer id ) {
        Optional<Pacotes> pacote = repository.findById( id );
        return pacote.orElse(null);
    }

}
