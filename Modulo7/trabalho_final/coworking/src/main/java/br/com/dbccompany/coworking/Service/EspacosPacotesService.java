package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository repository;


    @Transactional( rollbackFor = Exception.class )
    public EspacosPacotes salvar( EspacosPacotes espacosPacotes ) {
        return repository.save( espacosPacotes );
    }

    @Transactional( rollbackFor = Exception.class)
    public EspacosPacotes editar( EspacosPacotes espacosPacotes, Integer id ) {
        espacosPacotes.setId( id );
        return repository.save( espacosPacotes );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<EspacosPacotes> buscarTodosRegistros() {
        return (List<EspacosPacotes>) repository.findAll();
    }

    public EspacosPacotes buscarRegistroPorId( Integer id ) {
        Optional<EspacosPacotes> espacosPacotes = repository.findById( id );
        return espacosPacotes.orElse(null);
    }

}
