package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Usuarios salvar(Usuarios usuario ) {
        return repository.save( usuario );
    }

    @Transactional( rollbackFor = Exception.class)
    public Usuarios editar( Usuarios usuario, Integer id ) {
        usuario.setId( id );
        return repository.save( usuario );
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public List<Usuarios> buscarTodosRegistros() {
        return (List<Usuarios>) repository.findAll();
    }

    public Usuarios buscarRegistroPorId( Integer id ) {
        Optional<Usuarios> usuario = repository.findById( id );
        return usuario.orElse(null);
    }
}
