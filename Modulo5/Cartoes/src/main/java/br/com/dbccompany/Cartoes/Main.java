package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.LojaXCredenciador;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();

			// Lojas
			LojaEntity loja1 = new LojaEntity();
			LojaEntity loja2 = new LojaEntity();
			LojaEntity loja3 = new LojaEntity();
			loja1.setNome("Renner");
			loja2.setNome("Americanas");
			loja3.setNome("Submarino");
			session.save(loja1);
			session.save(loja2);
			session.save(loja3);
			// Lojas Array
			ArrayList<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja1);
			lojas.add(loja2);
			lojas.add(loja3);

			// Credenciadoras
			CredenciadorEntity cred1 = new CredenciadorEntity();
			CredenciadorEntity cred2 = new CredenciadorEntity();
			CredenciadorEntity cred3 = new CredenciadorEntity();
			cred1.setNome("Cred 1");
			cred2.setNome("Cred 2");
			cred3.setNome("Cred 3");
			session.save(cred1);
			session.save(cred2);
			session.save(cred3);
			// Credenciadoras Array
			ArrayList<CredenciadorEntity> credenciadoras = new ArrayList<>();
			credenciadoras.add(cred1);
			credenciadoras.add(cred2);
			credenciadoras.add(cred3);

			// Loja X Credenciador
			LojaXCredenciador lojaXCredenciador = new LojaXCredenciador();
			lojaXCredenciador.setLoja(lojas);
			lojaXCredenciador.setCredenciador(credenciadoras);
			lojaXCredenciador.setTaxa(5.0);
			session.save(lojaXCredenciador);

			// Bandeira
			BandeiraEntity bandeira1 = new BandeiraEntity();
			BandeiraEntity bandeira2 = new BandeiraEntity();
			bandeira1.setNome("VISA");
			bandeira1.setTaxa(10.0);
			bandeira2.setNome("MASTER");
			bandeira2.setTaxa(20.0);
			session.save(bandeira1);
			session.save(bandeira2);

			// Emissor
			EmissorEntity emissor1 = new EmissorEntity();
			EmissorEntity emissor2 = new EmissorEntity();
			emissor1.setNome("Em One");
			emissor1.setTaxa(10.0);
			emissor2.setNome("Em two");
			emissor2.setTaxa(12.0);
			session.save(emissor1);
			session.save(emissor2);

			// Cliente
			ClienteEntity cli1 = new ClienteEntity();
			ClienteEntity cli2 = new ClienteEntity();
			cli1.setNome("Rafael");
			cli2.setNome("Carol");
			session.save(cli1);
			session.save(cli2);

			// Cartao
			CartaoEntity card = new CartaoEntity();
			card.setBandeira(bandeira1);
			card.setChip(1234);
			card.setCliente(cli1);
			card.setEmissor(emissor2);
			card.setVencimento(new Date(2019, 02, 10));
			session.save(card);

			// Lancamento
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setCartao(card);
			lancamento.setData_compra(new Date(2020, 01, 12));
			lancamento.setDescricao("Pastel");
			lancamento.setEmissor(emissor2);
			lancamento.setLoja(loja3);
			lancamento.setValor(5.45);
			session.save(lancamento);

			transaction.commit();

			
			// nova transacao
			Session sessionTwo = HibernateUtil.getSessionFactory().openSession();
			sessionTwo.beginTransaction();
			
			// carrega dados
			LancamentoEntity lancamentoData = (LancamentoEntity) sessionTwo.get(LancamentoEntity.class, 1);
//			EmissorEntity emissorData = (EmissorEntity) sessionTwo.get(EmissorEntity.class, 1);
//			BandeiraEntity bandeiraData = (BandeiraEntity) sessionTwo.get(BandeiraEntity.class, 1);
//			LojaXCredenciador lojaXCredenciadorData = (LojaXCredenciador) sessionTwo.get(LojaXCredenciador.class, 1);
			
//			System.out.println(lanc.getId() + " desc " + lanc.getDescricao());
			//System.out.println(lancamentoData.getTotalEmissor());
			
			//System.out.println(lancamentoData.getValor() / (lancamentoData.getEmissor().getTaxa() / 100));
			System.out.println("Valor: "+lancamentoData.getValor());
			System.out.println("Taxa Emissor: "+lancamentoData.getEmissor().getTaxa()+"%");
			System.out.println("Taxa Bandeira: " + lancamentoData.getCartao().getBandeira().getTaxa()+"%");
			System.out.println("Taxa Credenciador: " + lancamentoData);
			// commita transacao
			sessionTwo.getTransaction().commit();
		       

		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			System.exit(1);
		} finally {
			System.exit(0);

		}

	}

}
