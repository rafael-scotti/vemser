package br.com.dbccompany.Cartoes.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CARTAO")
public class CartaoEntity {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ")
	@GeneratedValue(generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_CARTAO")
	private Integer id;
	
	@Column(name = "CHIP", nullable = false)
	private Integer chip;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_CLIENTE", nullable = false)
	private ClienteEntity cliente;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_BANDEIRA", nullable = false)
	private BandeiraEntity bandeira;
	
	@ManyToOne
	@JoinColumn(name = "FK_ID_EMISSOR", nullable = false)
	private EmissorEntity emissor;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "VENCIMENTO", nullable = false)
	private Date vencimento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChip() {
		return chip;
	}

	public void setChip(Integer chip) {
		this.chip = chip;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public BandeiraEntity getBandeira() {
		return bandeira;
	}

	public void setBandeira(BandeiraEntity bandeira) {
		this.bandeira = bandeira;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}
	
	
	
}
