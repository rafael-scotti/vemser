const circulo = {
    raio : 5,
    tipoCalculo: "A"
};

// 01
function calcularCirculo(circulo) {
    let {raio, tipoCalculo} = circulo;
    if(tipoCalculo == "A") {
        return Math.PI * (raio*raio);
    }else if (tipoCalculo == "C") {
        return Math.PI * raio * 2;
    }
}

console.log("#1   CalcularCirculo " + calcularCirculo(circulo));

// 02
/* function naoBissexto(ano) {
    if( ano % 400 == 0 ||  (ano % 4 == 0 && ano % 100 != 0 )) {
        console.log("É Bissesto.");
        return false;
    }else{
        console.log("Nao Bissesto.");
        return true;
    }
} */

let bissexto = ano => (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0);

console.log("#2   Bissexto: " + bissexto(2016));

// 03
function somarPares(array) {
    let total = 0;
    for(let i = 0; i < array.length; i++) {
        i % 2 == 0 ? total += array[i] : 0;
    }
    return total;
}

console.log("#3   SomarPares " + somarPares([1, 56, 4.34, 6, -2]));

// 04
/* function adicionar(numero1){
    let v2 = function(numero2) {
        return numero1+numero2;
    }
    return v2;
} */
let adicionar = op1 => op2 => op1 + op2; // curry?

console.log("#4   adicionar " + adicionar(2)(3));

// 05
let moedas = ( function() {
    // tudo é privado
    function imprimirMoeda( params ) {
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow( 10, precisao );
            return Math.ceil( numero * fator ) / fator;
        }

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params

        let qtdCasasMilhares = 3
        let StringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero)%1)
        let parteInteira = Math.trunc(numero)
        let parteInteiraString = Math.abs(parteInteira).toString()
        let parteInteiraTamanho = parteInteiraString.length

        let c = 1
        while(parteInteiraString.length > 0) {
            if(c % qtdCasasMilhares == 0) {
                StringBuffer.push( `${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c )}` )
                parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
            }else if(parteInteiraString.length <= qtdCasasMilhares) {
                StringBuffer.push(parteInteiraString)
                parteInteiraString = ''
            }
            c++
        }

        StringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, 0)
        const numeroFormatado = `${ StringBuffer.reverse().join('') }${ separadorDecimal }${ decimalString }`

        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
    }

    // tudo é publico
    return {
        imprimirBRL: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-R$${ numeroFormatado }`
            }),
        imprimirGBP: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-£${ numeroFormatado }`
            }),
        imprimirFR: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `${ numeroFormatado } €`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
            })
    }
})()


console.log("#5   " + moedas.imprimirBRL(2313477.0135));

console.log("#6   " + moedas.imprimirGBP(2313477.0135));

console.log("#7   " + moedas.imprimirFR(2313477.0135));