let multiplicar = (multiplicador, ...multiplicados) => multiplicados.map(valor => valor * multiplicador);

console.log(multiplicar(5, 3, 4));
