let nome = document.getElementById('nome');
let email = document.getElementById('email');
let telefone = document.getElementById('telefone');
let assunto = document.getElementById('exampleFormControlTextarea1');
let form = document.getElementById('my-form');

nome.addEventListener('blur', () => {
    if(!isValidName()) {
        alert("O campo Nome deve possuir pelo menos 10 caracteres!");
        nome.style.borderColor = "#ff0000";
        nome.blur();
    } else {
        nome.style.borderColor = "#00ff00";
    }
});

email.addEventListener('blur', () => {
    if(!isValidEmail()) {
        alert("Email invalido!");
        email.style.borderColor = "#ff0000";
        email.blur();
    } else {
        email.style.borderColor = "#00ff00";
    }
});

form.addEventListener('submit', () => {
    if(!isValidName()) {
        nome.style.borderColor = "#ff0000";
        nome.focus();
    } else if (!isValidEmail()) {
        email.style.borderColor = "#ff0000";
        email.focus();
    }
    validaForm(nome, email, telefone, assunto);
})

let isValidName = () => {
    return (nome.value.length > 10)
}

let isValidEmail = () => {
    return email.value.includes('@');
    
}

let validaForm = (...elements) => elements
                                    .filter(element => element.value == '')
                                    .map(element => {                         
                                        event.preventDefault();
                                        element.style.borderColor = '#ff0000';                 
                                    })