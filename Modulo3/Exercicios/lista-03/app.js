function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
    
    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }

    cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']

/*     let arr = cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] ) */
    
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      let indiceEnroladinho = cardapio.indexOf( 'enroladinho de salsicha' )
      cardapio.splice( indiceEnroladinho, 1 )
      let indicePastelCarne = cardapio.indexOf( 'pastel de carne' )
      cardapio.splice( indicePastelCarne, 1 )
    }

    let resultado = cardapio.map(alimento => alimento.toUpperCase());

/*     let resultadoFinal = []
    let i = 0;
    while (i <= cardapio.length - 1) {
      resultadoFinal[i] = cardapio[i].toUpperCase()
      i++
    } */
    return resultado;
  }
console.log(  cardapioIFood() ); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]