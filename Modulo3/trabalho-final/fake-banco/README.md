# Fake Bank
## Available Scripts

In the project directory, run:
### `npm install`
and then:
### `npm start`
 
It will runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

To run the project properly, you must clone and initialize the api-fake-bank, hosted in <https://github.com/marcoshs90/api-fake-banco.git>.