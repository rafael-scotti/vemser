import React, { Component } from 'react';
import Header from '../../components/header/Header'
import { Container } from 'react-bootstrap';
import Api from '../../services/Api'
import { Table } from 'react-bootstrap';
import Input from '../../components/input/Input'

export default class TiposConta extends Component {
  constructor( props ) {
    super( props );
    this.api = new Api();
    this.state = {
      tiposConta: []
    }
  }

  componentDidMount() {
    this._asyncRequest = this.requestTiposConta();
    this._asyncRequest = null;
  }

  componentWillUnmount() {
    if ( this._asyncRequest ) {
      this._asyncRequest.cancel();
    }
  }

  requestTiposConta = () => {
    return this.api.getTiposConta()
      .then( value => this.setState({ tiposConta : value.data.tipos }) )
      .catch( "Nao foi possivel carregar o conteúdo" )
  }

  searchFilter = evt => {
    let nome = evt.target.value.toUpperCase();
    return this.api.getTiposConta()
      .then( value => this.setState({ tiposConta: value.data.tipos.filter(tc => tc.nome.toUpperCase().includes(nome)) }) )
      .catch( "Nenhum resultado encontrado" )
  }

  render() {
    const { tiposConta } = this.state
    return <div>
      <Header />
      <Container className="container">
        <h1>
          Tipos Conta
        </h1>
        <Input type='text' placeholder='Pesquisar tipos de contas' onChange={ this.searchFilter } />
        <Table className={ 'my-table table-hover' }>
          <thead className='thead-dark'>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nome</th>
            </tr>
          </thead>
          <tbody>
            {
              tiposConta.map( tc => {
                return <tr key={ tc.id }>
                  <th scope="col">{ tc.id }</th>
                  <th scope="col">{ tc.nome }</th>
                </tr>
              })
            }
          </tbody>
        </Table>
      </Container> 
    </div>;
  }
}
