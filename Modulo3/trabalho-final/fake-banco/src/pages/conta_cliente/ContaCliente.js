import React, { Component } from 'react';
import Header from '../../components/header/Header'
import { Container } from 'react-bootstrap';
import Api from '../../services/Api'
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Input from '../../components/input/Input'

export default class ContaCliente extends Component {
  constructor( props ) {
    super( props );
    this.api = new Api();
    this.state = {
      contaClientes: []
    }
  }

  componentDidMount() {
    this._asyncRequest = this.requestContaClientes();
    this._asyncRequest = null;
  }

  componentWillUnmount() {
    if (this._asyncRequest) {
      this._asyncRequest.cancel();
    }
  }

  requestContaClientes = () => {
    return this.api.getContaClientes()
      .then( value => this.setState({ contaClientes: value.data.cliente_x_conta }) )
      .catch( "Nao foi possivel carregar o conteúdo" )
  }

  searchFilter = evt => {
    let nome = evt.target.value.toUpperCase();
    return this.api.getContaClientes()
      .then( value => this.setState({ contaClientes: value.data.cliente_x_conta.filter( tc => tc.tipo.nome.toUpperCase().includes( nome ) ) }) )
      .catch( "Nenhum resultado encontrado" )
  }

  render() {
    const { contaClientes } = this.state
    return <div>
      <Header />
      <Container className="container">
        <h1>
          Conta Clientes
        </h1>
        <Input type='text' placeholder='Pesquisar contas cliente' onChange={ this.searchFilter } />
        <div className="table-responsive">
          <Table className={ 'my-table table-hover ' }>
            <thead className='thead-dark'>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Conta</th>
                <th scope="col">Cliente</th>
              </tr>
            </thead>
            <tbody>
              {
                contaClientes.map( tc => {
                  return <tr key={ tc.id }>
                    <th scope="col">{ tc.id }</th>
                    <th scope="col">{ tc.tipo.nome }</th>
                    <th scope="col">{ <Link to={{ pathname: `/conta/cliente/${ tc.id }` }}>{ tc.cliente.nome }</Link> }</th>
                  </tr>
                })
              }
            </tbody>
          </Table>
        </div>
      </Container>
    </div>;
  }
}
