import React, { Component } from 'react';
import Cliente from './Cliente';
import { Container } from 'react-bootstrap';
import Header from '../../components/header/Header';
import Api from '../../services/Api';
import { Table } from 'react-bootstrap';
import '../../css/style.css'
import { Link } from 'react-router-dom';
import Input from '../../components/input/Input';

export default class ListaCliente extends Component {
  constructor( props ) {
    super( props );
    this.api = new Api();
    this.state = {
      clientes: []
    }
  }

  componentDidMount() {
    this._asyncRequest = this.requestClientes();
    this._asyncRequest = null;
  }

  componentWillUnmount() {
    if ( this._asyncRequest ) {
      this._asyncRequest.cancel();
    }
  }

  requestClientes = () => {
    return this.api.getClientes()
      .then( value => this.setState( {
        clientes: value.data.clientes.map( ag => ag = new Cliente(
          ag.id,
          ag.nome,
          ag.cpf,
          ag.agencia,
        ))
      }))
      .catch( "Nao foi possivel carregar o conteúdo" )
  }

  searchFilter = evt => {
    let nome = evt.target.value.toUpperCase();
    return this.api.getClientes()
      .then( value => this.setState( { clientes: value.data.clientes.filter(cli => cli.nome.toUpperCase().includes( nome ) ) } ) )
      .catch( "Nenhum resultado encontrado" )
  }

  render() {
    const { clientes } = this.state
    return (
      <React.Fragment>
        <Header logged={ true } />
        <Container className="container">
          <h1> Clientes </h1>
          <Input type='text' placeholder='Pesquisar cliente' onChange={ this.searchFilter } />
          <Table className={ 'my-table table-hover' }>
            <thead className='thead-dark'>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
              </tr>
            </thead>
            <tbody>
              {
                clientes.map( cliente => {
                  return <tr key={ cliente.id }>
                    <th scope="col">{ cliente.id }</th>
                    <th scope="col">{ <Link to={{ pathname: `/cliente/${cliente.id}` }}>{ cliente.nome }</Link> }</th>
                    <th scope="col">{ cliente.cpf }</th>
                  </tr>
                })
              }

            </tbody>
          </Table>

        </Container>
      </React.Fragment>
    );
  }
}