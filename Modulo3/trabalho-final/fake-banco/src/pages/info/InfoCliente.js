import React, { Component } from 'react';
import Api from '../../services/Api';
import Header from '../../components/header/Header';
import { Container, Card, ListGroup, Row, Col } from 'react-bootstrap';

export default class InfoCliente extends Component {
  constructor( props ) {
    super( props )
    this.api = new Api();
    this.state = {
      cliente: {
        agencia: {
          endereco: {}
        }
      },
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.requestCliente(id - 1)
  }

  requestCliente(id) {
    return this.api.getCliente(id)
      .then(value => this.setState({
        cliente: value.data.cliente
      }))
  }

  render() {
    const { cliente } = this.state
    return (
      <div>
        <Header />
        <Container>
          <h1>Cliente {cliente.nome}</h1>
          <Row>
            <Col lg={4}>
              <div>
                <Card style={{ maxWidth: '18rem' }}>
                  <Card.Header as="h5">Cliente</Card.Header>
                  <ListGroup variant="flush">
                    <ListGroup.Item>ID: { cliente.id }</ListGroup.Item>
                    <ListGroup.Item>Nome: { cliente.nome }</ListGroup.Item>
                    <ListGroup.Item>CPF: { cliente.cpf }</ListGroup.Item>
                  </ListGroup>
                </Card>
              </div>
            </Col>
            <Col lg={8}>
              <div>
                <Card style={{ maxWidth: '42rem' }}>
                  <Card.Header as="h5">Agencia</Card.Header>
                  <ListGroup variant="flush">
                    <ListGroup.Item>ID: { cliente.agencia.id }</ListGroup.Item>
                    <ListGroup.Item>Codigo: { cliente.agencia.codigo }</ListGroup.Item>
                    <ListGroup.Item>Nome: { cliente.agencia.nome }</ListGroup.Item>
                  </ListGroup>
                  <Card.Footer className="text-muted">Logradouro { cliente.agencia.endereco.logradouro } Nº{ cliente.agencia.endereco.numero }, Bairro { cliente.agencia.endereco.bairro }, { cliente.agencia.endereco.cidade } - { cliente.agencia.endereco.uf }</Card.Footer>
                </Card>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}
