import React, { Component } from 'react';
import Header from '../../components/header/Header';
import { Container, Card, ListGroup, Col } from 'react-bootstrap';
import Api from '../../services/Api';
import { Row } from 'react-bootstrap';

export default class infoContaCliente extends Component {
  constructor( props ) {
    super( props )
    this.api = new Api();
    this.state = {
      contaCliente: {},
      tipo: {},
      cliente: {},
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.requestContaCliente( id - 1 )
  }

  requestContaCliente( id ) {
    return this.api.getContaCliente( id )
      .then( value => this.setState({
        contaCliente: value.data.conta,
        tipo: value.data.conta.tipo,
        cliente: value.data.conta.cliente
      }) )
  }

  render() {
    const { contaCliente, tipo, cliente } = this.state
    return (
      <div>
        <Header />
        <Container>
          <h1>Conta cliente</h1>
          <div>
            <h3>ID: { contaCliente.id } COD: { contaCliente.codigo } </h3>
          </div>
          <Row className="justify-content-center">
            <Col md={4}>
              <div>
                <Card style={{ marginTop: '20px', width: '18rem' }}>
                  <Card.Header as="h5">Conta</Card.Header>
                  <ListGroup variant="flush">
                    <ListGroup.Item>ID: { tipo.id }</ListGroup.Item>
                    <ListGroup.Item>Tipo: { tipo.nome }</ListGroup.Item>
                  </ListGroup>
                </Card>
              </div>
            </Col>
            <Col md={4}>
              <div>
                <Card style={{ marginTop: '20px', width: '18rem' }}>
                  <Card.Header as="h5">Cliente</Card.Header>
                  <ListGroup variant="flush">
                    <ListGroup.Item>ID: { cliente.id }</ListGroup.Item>
                    <ListGroup.Item>Nome: { cliente.nome }</ListGroup.Item>
                    <ListGroup.Item>Nome: { cliente.cpf }</ListGroup.Item>
                  </ListGroup>
                </Card>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
