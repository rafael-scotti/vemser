import React, { Component } from 'react';
import Header from '../../components/header/Header';
import { Container, Card, ListGroup } from 'react-bootstrap';
import Api from '../../services/Api';

export default class InfoAgencia extends Component {
  constructor( props ) {
    super( props )
    this.api = new Api();
    this.state = {
      agencia: {},
      endereco: {}
    }
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.requestAgencia( id - 1 )
  }

  requestAgencia( id ) {
    return this.api.getAgencia( id )
      .then(value => this.setState({ 
        agencia: value.data.agencias,
        endereco: value.data.agencias.endereco 
      }))
  }

  render() {
    const { agencia, endereco } = this.state
    return (
      <div>
        <Header />
        <Container>
          <h1>Agencia { agencia.nome }</h1>

          <div>
            <h3>ID: { agencia.id } COD: { agencia.codigo }</h3>
          </div>

          <div>
            <Card>
              <Card.Header as="h5">Endereço</Card.Header>
              <ListGroup variant="flush">
                <ListGroup.Item>Logradouro { endereco.logradouro } Nº{ endereco.numero }, Bairro { endereco.bairro }, { endereco.cidade } - { endereco.uf } </ListGroup.Item>
              </ListGroup>
            </Card>
          </div>
        </Container>
      </div>
    );
  }
}
