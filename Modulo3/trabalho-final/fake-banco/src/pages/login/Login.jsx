import React, { Component } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import InputLogin from '../../components/input/InputLogin';
import logo from '../../logo.svg';
import './login.css'
import Header from '../../components/header/Header'
import Button from '../../components/button/ButtonLogin';
import Api from '../../services/Api'
import customLogin from './custom-login-img.svg';
import { login } from '../../services/auth';

export default class Login extends Component {
  constructor( props ) {
    super( props );
    this.api = new Api()
    this.state = {
      email: "",
      password: "",
      error: ""
    };
  }

  handleSignIn = async e => {
    e.preventDefault();
    const { email, password } = this.state;
    if( !email || !password ) {
      this.setState({ error: "Voce deve preencher todos os campos!" });
    }else {
      try {
        const response = await this.api.postLogin( email, password )
        login( response.data.token );
        this.props.history.push( "/home" );
      } catch ( err ) {
        this.setState({
          error: "Usuario ou senha invalidos!"
        });
      }
    }
  };

  render() {
    return (
     <section>
        <Header logged={ false } />
        <div className="main-login">
          <Container >
            <Row className="justify-content-center">
              <Col md={6} className="col-img-area">
                <div className="custom-img-area">
                  <img className="img" src={customLogin} alt="password door"/>
                </div>
              </Col>
              <Col md={5}>
                <div className='logo'>
                  <img src={logo} alt="" />
                </div>
                <Form onSubmit={this.handleSignIn}>
                  <div className='login-form'>
                    <InputLogin type='text' onChange={ e => this.setState({ email: e.target.value }) } placeholder='Username' />
                  </div>
                  <div className='login-form'>
                    <InputLogin type='password' onChange={ e => this.setState({ password: e.target.value }) } placeholder='Password' />
                  </div>
                  <div className='login-form'>
                    <Button type='submit' label="Login" />
                  </div>
                </Form>
                {this.state.error && <p id="error">{this.state.error}</p>}
              </Col>
            </Row>
          </Container>
        </div>
     </section>
      

    )
  }
}