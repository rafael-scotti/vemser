import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import Header from '../../components/header/Header';
import { Link } from 'react-router-dom';
import './home.css'
import logoAgencia from './agencia.svg';
import logoCliente from './cliente.svg';
import logoTipoConta from './tipo_conta.svg';
import logoContaCliente from './conta_cliente.svg'

export default class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <Container>
          <h1> Home </h1>
          <div className='prof'>
            <div className="row">
              <div className="col-6 col-lg-3">
                <img className="rounded-circle" src={logoAgencia} alt="Agencia" width="140" height="140" />
                <h2>Agencias</h2>
                <p><Link className="btn btn-secondary" to='/agencias' role="button">Ver detalhes &raquo;</Link></p>
              </div>

              <div className="col-6 col-lg-3">
                <img className="rounded-circle" src={logoCliente} alt="Cliente" width="140" height="140" />
                <h2>Clientes</h2>
                <p><Link className="btn btn-secondary" to='/clientes' role="button">Ver detalhes &raquo;</Link></p>
              </div>

              <div className="col-6 col-lg-3">
                <img className="rounded-circle" src={logoTipoConta} alt="Tipos conta" width="140" height="140" />
                <h2>Tipos de conta</h2>
                <p><Link className="btn btn-secondary" to='/tipos-conta' role="button">Ver detalhes &raquo;</Link></p>
              </div>

              <div className="col-6 col-lg-3">
                <img className="rounded-circle" src={logoContaCliente} alt="Conta clientes" width="140" height="140" />
                <h2>Contas cliente</h2>
                <p><Link className="btn btn-secondary" to='/conta/clientes' role="button">Ver detalhes &raquo;</Link></p>
              </div>
            </div>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}
