import React, { Component } from 'react';
import { Container, Table } from 'react-bootstrap';
import Api from '../../services/Api';
import Header from '../../components/header/Header'
import Agencia from './Agencia.js';
import './agencia.css';
import '../../css/style.css'
import { Link } from 'react-router-dom';
import Input from '../../components/input/Input';

export default class ListaAgencia extends Component {
  constructor( props ) {
    super( props );
    this.api = new Api();
    this.state = {
      agencias: []
    }
  }

  componentDidMount() {
    this._asyncRequest = this.requestAgencias();
    this._asyncRequest = null;
  }

  componentWillUnmount() {
    if ( this._asyncReques ) {
      this._asyncRequest.cancel();
    }
  }

  requestAgencias = () => {
    return this.api.getAgencias()
      .then( value => this.setState({
        agencias: value.data.agencias.map( ag => ag = new Agencia(
          ag.id,
          ag.codigo,
          ag.nome,
          ag.endereco.logradouro,
          ag.endereco.numero,
          ag.endereco.bairro,
          ag.endereco.cidade,
          ag.endereco.uf,  
        ))}))
      .catch( "Nao foi possivel carregar o conteúdo" )
  }

  searchFilter = evt => {
    let nome = evt.target.value.toUpperCase();
    return this.api.getAgencias()
      .then( value => this.setState({ agencias: value.data.agencias.filter( ag => ag.nome.toUpperCase().includes( nome ) )}))
      .catch( "Nenhum resultado encontrado" )
  }

  setDigital = (agencia)=> {
    const { agencias } = this.state
    let curr = agencia.isDigital;
    let dados = agencias;
    dados.find( ag => ag === agencia ).isDigital = !curr;
    this.setState({ agencias: dados })
  }

  searchDigital = () => {
    let checkDigital = document.getElementById( 'digital' );
    const { agencias } = this.state;
    if ( checkDigital.checked ) {
      return this.setState({ agencias: agencias.filter( ag => ag.isDigital === true ) })
    } else {
      this.requestAgencias()
    }
  }

  render() {
    const { agencias } = this.state
    return (
      <React.Fragment>
        <Header logged={ true } />
        <Container className="container">
          <h1> Agencias </h1>
          <Input type='text' placeholder='Pesquisar agencia' onChange={ this.searchFilter }/>
          <div htmlFor="digital" onClick={ this.searchDigital }>
            <input type='checkbox' id="digital" name="digital" />
            <label >Digital</label>
          </div>

          <Table className={ 'my-table table-hover' }>
            <thead className='thead-dark'>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col">Digital</th>
              </tr>
            </thead>
            <tbody>
              {
                agencias.map( agencia => {
                  return <tr key={ agencia.id }>
                    <th scope="col">{ agencia.id }</th>
                    <th scope="col"><Link to={{ pathname: `/agencia/${ agencia.id }` }}>{ agencia.nome }</Link></th>
                    <th scope="col">
                      <label className="container-checkbox" >
                        <input type="checkbox"  />
                          <span className="checkmark"></span>
                      </label>                     
                    </th>
                  </tr>
                })
              }
            </tbody>
          </Table>
        </Container>
      </React.Fragment>
    );
  }
}
