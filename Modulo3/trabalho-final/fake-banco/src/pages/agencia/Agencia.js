export default class Agencia {
  constructor( id, codigo, nome, logradouro, numero, bairro, cidade, uf ) {
    this.id = id
    this.codigo = codigo
    this.nome = nome
    this.logradouro = logradouro
    this.numero = numero
    this.bairro = bairro
    this.cidade = cidade
    this.uf = uf
  }

  toString() {
    return ( `id: ${ this.id } cod: ${ this.codigo } nome: ${ this.nome } log: ${ this.logradouro } numero: ${ this.numero } bairro: ${ this.bairro } cidade ${ this.cidade }` )
  }
}
