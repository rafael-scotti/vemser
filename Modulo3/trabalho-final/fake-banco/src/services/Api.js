import axios from 'axios';
// import config from '../configs/config.json'
import { getToken } from './auth';

export default class Api {

  constructor() {
    this.api = axios.create({
      baseURL: "http://localhost:1337"
    })
    
    this.api.interceptors.request.use(async config => {
      const token = getToken();
      if (token) {
        config.headers.Authorization = `${token}`
      }
      return config;
    })
    
  }

  getAgencias = () => this.api.get('/agencias', { 
    headers: { 
      Authorization: getToken() 
    } 
  })

  getClientes = () => this.api.get('/clientes', {
    headers: {
      Authorization: getToken()
    }
  })

  getCliente = (id) => this.api.get(`/cliente/${id}`, {
    headers: {
      Authorization: getToken()
    }
  })

  getAgencia = (id) => this.api.get(`/agencia/${id}`, {
    headers: {
      Authorization: getToken()
    }
  })

  getTiposConta = () => this.api.get('/tipoContas/', {
    headers: {
      Authorization: getToken()
    }
  })

  getTipoConta = (id) => this.api.get(`/tiposConta/${id}`, {
    headers: {
      Authorization: getToken()
    }
  })

  getContaClientes = () => this.api.get('/conta/clientes/', {
    headers: {
      Authorization: getToken()
    }
  })

  getContaCliente = (id) => this.api.get(`/conta/cliente/${id}`, {
    headers: {
      Authorization: getToken()
    }
  })

  postLogin = (email, password) => this.api.post("/login", { "email": email, "senha":password });

}