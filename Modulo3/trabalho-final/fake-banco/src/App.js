import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Login from './pages/login/Login'
import Home from './pages/home/Home';
import ListaAgencia from './pages/agencia/ListaAgencia';
import ListaCliente from './pages/cliente/ListaCliente';
import InfoCliente from './pages/info/InfoCliente'
import InfoAgencia from './pages/info/infoAgencia';
import TiposConta from './pages/tipos_conta/TiposConta';
import ContaCliente from './pages/conta_cliente/ContaCliente';

import { isAuthenticated } from './services/auth';
import infoContaCliente from './pages/info/infoContaCliente';

function App() {

  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props => isAuthenticated() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{ pathname: '/', state: { from: props.location } }} />
        )
      }
    />
  )

  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={ isAuthenticated() ? Home : Login} />
        <PrivateRoute path='/home' exact component={Home} />
        <PrivateRoute path='/agencias' component={ListaAgencia} />
        <PrivateRoute path='/clientes' component={ListaCliente} />
        <PrivateRoute path='/cliente/:id' component={InfoCliente} />
        <PrivateRoute path='/agencia/:id' component={InfoAgencia} />
        <PrivateRoute path='/tipos-conta' component={TiposConta} />
        <PrivateRoute path='/conta/clientes' component={ContaCliente} />
        <PrivateRoute path='/conta/cliente/:id' component={infoContaCliente} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
