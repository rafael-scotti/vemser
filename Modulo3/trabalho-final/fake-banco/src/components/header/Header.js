import React, { Component } from 'react';
import './header.css';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class Header extends Component {
  render() {
    const { logged } = this.props;
    return (
      <div className='main-header'>
        <Container>
          <Navbar expand="lg" className="responsive-menu-icon" >
           <Link to="/home"> <Navbar.Brand>Fake Bank</Navbar.Brand></Link>
            {
              logged ? (
                <React.Fragment>
                  <Navbar.Toggle aria-controls="responsive-navbar-nav"  />
                  <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ml-auto">
                      <Link className="nav-link" to='/agencias'>Agências</Link>
                      <Link className="nav-link" to='/clientes'>Clientes</Link>
                      <Link className="nav-link" to='/tipos-conta'>Tipo de contas</Link>
                      <Link className="nav-link" to='/conta/clientes'>Conta de clientes</Link>
                    </Nav>
                  </Navbar.Collapse>
                </React.Fragment>
              ) : null
            }
          </Navbar>
        </Container>
      </div>
    );
  }
}

Header.propTypes = {
  logged: PropTypes.bool,
}

Header.defaultProps = {
  logged: true
}