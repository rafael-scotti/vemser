import React, { Component } from 'react';
import './input.css';
// import { Container } from './styles';

export default class InputLogin extends Component {
  render() {
    const { type, placeholder, onChange } = this.props
    return (
      <React.Fragment>
        <input className="main-input" type={type} onChange={onChange} placeholder={placeholder}></input>
      </React.Fragment>
    );
  }
}
