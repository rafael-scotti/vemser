import React, { Component } from 'react';
import './input.css';
// import { Container } from './styles';

export default class Input extends Component {
    render() {
        const { type, placeholder, onChange } = this.props
        return (
            <React.Fragment>
                <input className="search-input" type={type} onChange={onChange} placeholder={placeholder}></input>
            </React.Fragment>
        );
    }
}
