import React, { Component } from 'react';
import './button.css';
import PropTypes from 'prop-types';

export default class Button extends Component {
  render() {
    const { onClick, label, color, type } = this.props;
    return (
      <div className='main-button'>
        <button type={type} className={`${ color }`} onClick={onClick}> { label } </button>
      </div>
    );
  }
}

Button.propTypes = {
  label: PropTypes.string,
  color: PropTypes.string,
  type: PropTypes.string
}

Button.defaultProps = {
  label: "Botão",
  type: "text"
}