import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './Home';

export default class App extends Component {
  
  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/teste" component={ PaginaTeste } />
      </Router>
    );
  }
}

const PaginaTeste = () => (
  <div>
    <h1>Pagina Teste</h1>
    <Link to="/">Home</Link>
    <Link to="/teste">Teste</Link>
  </div>
);