import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return (
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome }></img>
                <h4>Duração: { episodio.duracaoEmMinutos }</h4>          
                <span> Episodio assistido: { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</span>
                <span>Temporada / Episodio: { episodio.temporadaEpisodio }</span>
            </React.Fragment>
        )
    }
}