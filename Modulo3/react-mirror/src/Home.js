import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './components/episodioUi'

class Home extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  atualizaMensagem = msg => {    
    this.setState({
      message: msg
    })

    setTimeout(() => {
      this.setState({ message: '' })
    },5000)
  };


/*   avaliar = () => {
    let nota = document.getElementById('nota').value;
    if(nota > 5){
      nota = 5;
    }else  if(nota < 1) {
      nota =1
    }
    const { episodio } = this.state
    this.listaEpisodios.avaliar( episodio, nota )
    this.setState({
      episodio
    })
    this.atualizaMensagem("Nota registrada com sucesso")
  } */

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState({
      episodio,
      deveExibirMensagem: true
    })
    setTimeout(() => {
      this.setState( {
        deveExibirMensagem: false
      })
    }, 5000)
  }

  geraCampoDeNota() {
    return (
      <div>
        {
        this.state.episodio.assistido && (
          <div className="avaliar">
            <span>Nota: { this.state.episodio.nota || 0 }</span>
              <input className="input-nota" type="number" id="nota" min='0' max='5' placeholder="Nota" onBlur = { this.registrarNota.bind(this) }></input>          
          </div> 
        )
      }
      </div>
    )
  }

  render() {
    const { episodio, deveExibirMensagem } = this.state;

    return (
      <div className="App">
        <header className="App-header">
        {/* { deveExibirMensagem ? ( <span> Registramos sua nota! </span> ) : ''} */}
        <span className={`flash verde ${ deveExibirMensagem ? '' : 'invisivel'}` }>Registramos sua nota!</span>
          
          <EpisodioUi episodio={ episodio } />

          <div>
              <button className="btn" onClick = { this.assistido.bind( this ) }>Ja assisti</button>
              <button className="btn" onClick = { this.sortear }>Próximo</button>
          </div>
          
          { this.geraCampoDeNota() }
          
        </header>
      </div>
    );
  }
}


export default Home;
