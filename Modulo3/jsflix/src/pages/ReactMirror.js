import React, { Component } from 'react';
import '../App.css';
import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../components/episodioUi'
import Header from '../components/header';
import MensagemFlash from '../components/MensagemFlash';
import MeuInputNumero from '../components/MeuInputNumero';
import Botao from '../components/Botao';
import { Link } from 'react-router-dom'

class Home extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota(evt) {
    const nota = evt.target.value
    const { episodio } = this.state
    let cor, mensagem
    if (episodio.notaValida(nota)) {
      episodio.avaliar(nota)
      mensagem = "Nota atualizada!";
      cor = 'verde';
    } else {
      mensagem = `Nota invalida! `;
      cor = 'vermelho';
    }

    this.exibirMensagem({cor, mensagem});
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor, 
      mensagem,
      exibirMensagem: true
    })
  }

  geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div className="avaliar">
              <span>Nota: {this.state.episodio.nota || 0}</span>
              <input className="input-nota" type="number" id="nota" placeholder="Nota" onBlur={this.registrarNota.bind(this)}></input>
            </div>
          )
        }
      </div>
    )
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor,mensagem } = this.state;
    const { listaEpisodios } = this
    return (
      <div className="App">
        <Header />


        <header className="App-header">

          <MensagemFlash atualizarMensagem={this.atualizarMensagem}
            deveExibirMensagem={exibirMensagem}
            mensagem={mensagem}
            cor={cor}
            duracao={5000} />

          <EpisodioUi episodio={episodio} />

          <div>
            <Botao value='Ja assisti' background='preto' onClick={this.assistido.bind(this)} />
            <Botao value='Proximo' background='preto' onClick={this.sortear} />
            <Botao value={<Link to={{ pathname: "/avaliacoes", state: { listaEpisodios } }}>Avaliacoes</Link> } background='preto' />
          </div>

          {/*  {this.geraCampoDeNota()} */}
          <MeuInputNumero placeholder={'Nota'}
            exibir={this.state.episodio.assistido}
            span={`Nota: ${this.state.episodio.nota || 0}`}
            obrigatorio={true}
            blur={this.registrarNota.bind(this)} />

        </header>

      </div>
    );
  }
}

export default Home;
