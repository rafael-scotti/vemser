import React, { Component } from 'react';
import Header from '../components/header';
import ListaSeries from '../models/ListaSeries';
import Botao from '../components/Botao';
import MeuInputNumero from '../components/MeuInputNumero'
import '../css/jsflix.css';
export default class Jsflix extends Component {
  constructor(props) {
    super(props);
    this.series = new ListaSeries();

    console.log("#1 - Series invalidas");
    console.log(this.series.invalidas());

    console.log("#2 - filtro por ano (2017)");
    console.log(this.series.filtrarPorAno(2017));

    console.log("#3 - busca por nome (Steven Yeun)");
    console.log(this.series.procurarPorNome("Steven Yeun"));

    console.log("#4 - media episodios");
    console.log(this.series.mediaDeEpisodios());

    console.log("#5 - total salarios (0)");
    console.log(this.series.totalSalario(0));

    console.log("#6A - quero genero (caos)");
    console.log(this.series.queroGenero("Caos"));

    console.log("#6B - quero titulo (The)");
    console.log(this.series.queroTitulo("The"));

    console.log('#7 - creditos (0)');
    console.log(this.series.creditos(0));

    console.log("#8 - ");
    console.log(this.series.filtrarElencoAbreviado());

    this.state = {
      label: 'Resultados'
    }
  }

  invalidas = () => {
    this.setState({
      label: this.series.invalidas()
    })
  }

  mediaDeEpisodios = () => {
    this.setState({
      label: "Media de episodios: " + this.series.mediaDeEpisodios()
    })
  }

  palavraSecreta = () => {
    this.setState({
      label: "Palavra secreta: " + this.series.filtrarElencoAbreviado()
    })
  }

  filtroPorAno = (evt) => {
    this.setState({
      label: `Filtro por ano (${evt.target.value}): ${this.series.filtrarPorAno(evt.target.value).map(serie => `Titulo: ${serie.titulo} Ano: ${serie.anoEstreia}`)}`
    })
  }

  buscaPorNome = evt => {
    this.setState({
      label: `Existe "${evt.target.value}" em algum elenco? ${this.series.procurarPorNome(evt.target.value) ? "Sim" : "Nao"}`
    })
  }

  totalSalario = evt => {
    let posicao = evt.target.value || 0
    this.setState({
      label: `O total gasto em salario na serie foi de ${this.series.totalSalario(posicao)}`
    })
  }

  queroGenero = evt => {
    this.setState({
      label: `Genero (${evt.target.value}): ${this.series.queroGenero(evt.target.value)}`
    })
  }

  queroTitulo = evt => {
    this.setState({
      label: `Titulo (${evt.target.value}): ${this.series.queroTitulo(evt.target.value)}`
    })
  }

  creditos = evt => {
    let posicao = evt.target.value || 0
    this.setState({
      label: `Creditos: ${this.series.creditos(posicao)}` 
    })
  }

  render() {
    return (
      <div>
        <Header />
        <div className='container-jsflix'>

          <section className='botao-section'>
            <Botao onClick={this.invalidas} background='preto' className={``} value='Series invalidas' />
            <Botao onClick={this.mediaDeEpisodios} background='preto' className={``} value='Media de episodios' />
            <Botao onClick={this.palavraSecreta} background='preto' className={``} value='Palavra secreta' />
          </section>
          <div className='conteudo'>
            <section className='left-aside'>
              <MeuInputNumero blur={this.filtroPorAno} exibir={true} span={'Filtro por Ano'} placeholder='Ano' />
              <MeuInputNumero blur={this.buscaPorNome} exibir={true} span={'Busca por nome'} placeholder='Nome' />
              <MeuInputNumero blur={this.totalSalario} exibir={true} span={'Total Salario'} placeholder='Posicao' />
              <MeuInputNumero blur={this.queroGenero} exibir={true} span={'Buscar por genero'} placeholder='Genero' />
              <MeuInputNumero blur={this.queroTitulo} exibir={true} span={'Buscar por titulo'} placeholder='Titulo' />
              <MeuInputNumero blur={this.creditos} exibir={true} span={'Exibir creditos'} placeholder='Posicao' />
            </section>

            <section className='resultado'>
              <p>{this.state.label}</p>
            </section>
          </div>

          {/* <section className='main-jsflix-menu'>
            <div className='botao-section'>

            </div>

            <input onBlur={this.filtroPorAno} className={``} placeholder='Filtro por ano'></input>
            <input onBlur={this.buscaPorNome} className={``} placeholder='Busca por nome '></input>
            <input onBlur={this.totalSalario} className={``} placeholder='Total salario'></ input>
            <input className={``} placeholder='Quero genero'></input >
            <input className={``} placeholder='Quero titulo'></input>
            <input className={``} placeholder='Creditos'></ input>

          </section>
          
          <section className='resultados'>
            <label>{this.state.label}</label>
          </section> */}


        </div>
      </div>

    )
  }
}
