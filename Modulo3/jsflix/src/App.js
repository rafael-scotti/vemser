import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Home from './pages/Home';
import ReactMirror from './pages/ReactMirror';
import Jsflix from './pages/Jsflix';
import ListaAvaliacoes from './components/ListaAvaliacoes';
import TelaDetalheEpisodio from './components/TelaDetalheEpisodio';

export default class App extends Component {
  
  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/react-mirror" component={ ReactMirror } />
        <Route path="/jsflix" component={ Jsflix } />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />
      </Router>
    );
  }
}
