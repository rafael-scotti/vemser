import React, { Component } from 'react';
import PropType from 'prop-types';

export default class MensagemFlash extends Component {
  constructor( props ) {
    super( props )
    this.idsTimeouts = []
  }

  fechar = () => {
    console.log('fechar');
    this.props.atualizarMensagem( false );
  }

  limparTimeouts() {
    this.idsTimeouts.forEach( clearTimeout )
  }

  componentWillUnmount() {
    clearTimeout(this.idTimeout)
    //this.limparTimeouts()
  }

  componentDidUpdate( prevProps ) {
    const { deveExibirMensagem, duracao } = this.props
    if( deveExibirMensagem !== false ) {
      this.idTimeout = setTimeout( () => {
        this.fechar();
      }, duracao)
      //this.idsTimeouts.push( novoIdTimeout )
    }
  }

  render() {
    const { deveExibirMensagem, mensagem, cor } = this.props;

/*     if(deveExibirMensagem){
      setTimeout(() => {
        this.props.atualizarMensagem(false);
      }, duracao);
    } */

    return (
      <span onClick={ this.fechar } className={`flash ${ (cor === 'verde' || cor === 'vermelho') ? cor : 'verde'} ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
    )
  }
}

MensagemFlash.PropType = {
  mensagem: PropType.string.isRequired,
  deveExibirMensagem: PropType.bool.isRequired,
  atualizarMensagem: PropType.func.isRequired,
  cor: PropType.string,
  duracao: PropType.number
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  duracao: 3000
}