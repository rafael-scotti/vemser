import React, { Component } from 'react';
import PropType from 'prop-types';
import '../css/MeuInputNumero.css';

export default class MeuInputNumero extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { placeholder, exibir, obrigatorio, span, blur } = this.props

    return exibir ? (
      <div className={` meu-input-numero `}>

        <div className={`div-span-input ${obrigatorio ? 'obrigatorio' : ''} `}>

          {
            span && (<span className={'span-input'}> {span} </span>)
          }

          <input type={'text'} placeholder={placeholder} className={`main-input`} onBlur={blur}></input>

        </div>

        {
          obrigatorio && (<span className={'span-obrigatorio'}>*Obrigatorio*</span>)
        }

      </div>
    ) : null;
  }
}

MeuInputNumero.PropType = {
  placeholder: PropType.string,
  exibir: PropType.bool,
  span: PropType.string,
  obrigatorio: PropType.bool.isRequired
}