import React, { Component } from 'react';
import PropType from 'prop-types';
import '../css/botao.css';

export default class Botao extends Component {

    render() {
        const {onClick, background, value} = this.props
        return (
            <React.Fragment>
                <button onClick={onClick} className={`botao ${background} `}>{ value }</button>
            </React.Fragment>
        ) 
    }
}

Botao.PropType = {
    background: PropType.string,
    value: PropType.string
}

Botao.defaultProps = {
    background: 'branco'
}