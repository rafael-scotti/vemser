import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

const ListaAvaliacoes = props => {
    const { listaEpisodios } = props.location.state
    return listaEpisodios.avaliados.map( ep => {
        return <li key={ep.id}>
                <Link to={{ pathname: ` /episodio${ep.id}`, state: { episodio: ep } }}>
                    {`${ ep.nome } - ${ ep.nota } |  S${ ep.temporada}E${ep.ordem}`}
                </Link>
              </li>
    })
}

export default ListaAvaliacoes