import React from 'react';
import { Link } from 'react-router-dom';

export default function header() {
  return (
    <header>
        <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/react-mirror">React Mirror</Link></li>
            <li><Link to="/jsflix">JsFlix</Link></li>
        </ul>
    </header>
  );
}
