import React, { Component } from 'react';

import Header from '../components/header';
import Footer from '../components/footer';
import Box from '../components/box';
import LinhasDeServicoDBC_Smartsourcing from '../img/Linhas-de-Serviço-DBC_Smartsourcing.png';

export default class Servicos extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div>
        <Header />
        <section className="container main-box">
          <div className="row">
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
            <div className="col col-12 col-md-6 col-lg-4">
              <Box>
                <div>[
                  <img src={LinhasDeServicoDBC_Smartsourcing} />
                </div>
                <h4>Smartsourcing</h4>
                <p>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minus ex incidunt molestiae, ratione odit saepe sequi vitae impedit quam.
                </p>
              </Box>
            </div>
          </div>
        </section>
                  
        <Footer />
        


      </div>
    );
  }
}