import React, { Component } from 'react';

import Header from '../components/header';
import Banner from '../components/banner';
import SectionInfo from '../components/section-info';
import Footer from '../components/footer';

export default class Home  extends Component{
  constructor( props ){
      super( props );
    }
  
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <SectionInfo />
        <Footer />
      </div>
    );
  }
}
