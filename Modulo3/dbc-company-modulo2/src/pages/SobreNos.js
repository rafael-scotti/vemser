import React, { Component } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import ItemAbout from '../components/ItemAbout';

import '../css/about-us.css';
import ManCircle from '../img/male-circle.png';

export default class SobreNos extends Component {


  render() {
    return (
      <div>
        <Header />
        
        <section className="container about-us">
          <ItemAbout img={ManCircle} title="Rafael">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
          </ItemAbout>
          <ItemAbout img={ManCircle} title="Scotti">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
          </ItemAbout>
          <ItemAbout img={ManCircle} title="Zanella">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
          </ItemAbout>
          <ItemAbout img={ManCircle} title="Rafael Scotti Zanella">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
          </ItemAbout>
        </section>

        <Footer />
      </div>

    );
  }
}