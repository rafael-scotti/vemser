import React, { Component } from 'react';
import Header from '../components/header';
import Footer from '../components/footer';
import Form from '../components/form';
import Map from '../components/map';

export default class Contato extends Component {

    render() {
        return (
            <div>
                <Header />
                <section className="container">
                    <div className="row">
                        <div className="col col-12 col-md-6 col-lg-6 ">
                            <Form />
                        </div>
                        <div className="col col-12 col-md-6 col-lg-6 ">
                            <Map />
                        </div>
                    </div>
                    
                </section>

                <Footer />
            </div>
        );
    }
}