import React, { Component } from 'react';
import './css/App.css';
import Home from './pages/Home';
import SobreNos from './pages/SobreNos';
import Servicos from './pages/Servicos';
import Contato from './pages/Contato';

import { BrowserRouter, Route} from 'react-router-dom';

export default class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <Route path="/" exact component={ Home } />
        <Route path="/sobre-nos" component={ SobreNos } />
        <Route path="/servicos" component={ Servicos } />
        <Route path="/contato" component={ Contato } />
      </BrowserRouter>
    );
  }
}
