import React, { Component } from 'react';
import '../css/box.css';
import '../css/buttons.css';

export default (props) => (
  // <article className="box">
  //   <div>
  //     <img src={ props.img } alt={ props.alt } />
  //   </div>
  //   <h4>{ props.titulo }</h4>
  //   <p> { props.paragrafo }</p>
  //   <a className="button button-blue" href="#">{ props.btnName } </a>
  // </article>

  <article className="box">
    { props.children }
  </article>
);