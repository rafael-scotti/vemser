import React, { Component } from 'react';
import '../css/banner.css';
import '../css/buttons.css';

export default class Banner extends Component {

  render() {
    return (
      <section className="main-banner">
        <article>
          <h1>Vem ser DBC</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat qui, debitis aut eligendi praesentium ipsa nostrum deserunt officiis sunt voluptatibus minus corporis fugiat porro, dolorem eveniet asperiores accusamus distinctio corrupti.
            </p>
          <a className="button button-big button-outline" href="#">Saiba mais</a>
{/*           <button className="button button-big button-blue">Saiba mais</button>
          <input className="button button-big button-green" type="button" value="Saiba mais"> */}
        </article>
      </section>
    );
  }
}