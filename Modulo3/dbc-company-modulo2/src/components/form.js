import React, { Component } from 'react';
import '../css/form.css';

export default class Form extends Component {
    render() {
        return (
            <section className="main-form">
                <h1>
                    Contato
                    </h1>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo ipsum maiores facere, quasi, sit aliquid obcaecati fugit tempora maxime, quo minus. Perferendis ducimus debitis tempore cum eum error, eveniet sed. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatem veniam a laborum delectus, deleniti ab iusto blanditiis ratione modi consequatur. Saepe perferendis temporibus laborum veritatis ipsam! Quae perspiciatis voluptates itaque.
                    </p>
                <form>
                    <div className="row">
                        <div className="col col-12">
                            <input className="input" type="text" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12">
                            <input type="text" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12">
                            <input type="text" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12">
                            <textarea></textarea>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col col-12">
                            <input className="button button-blue" type="submit" value="Enviar" />
                        </div>
                    </div>
                </form>
            </section>
        );
    }
}
