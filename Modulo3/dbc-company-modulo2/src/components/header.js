import React, { Component } from 'react';
import '../css/header.css'
import logo from '../img/logo-dbc-topo.png';

import { Link } from 'react-router-dom';

export default class Header extends Component {

  render() {
    return (
      <header className="main-header">
        <nav className="container clearfix">
          <Link className="logo" to="/" title="Pagina principal">
              <img src={ logo } alt="DBC Company"/>
          </Link>

          <label className="mobile-menu" htmlFor="mobile-menu">
              <span></span>
              <span></span>
              <span></span>
          </label>
          <input id="mobile-menu" type="checkbox"/>

          <ul className="clearfix">
              <li>
                  <Link to="/">Home</Link>
              </li>
              <li>
                  <Link to="/sobre-nos">Sobre nós</Link>
              </li>
              <li>
                  <Link to="/servicos">Serviços</Link>
              </li>
              <li>
                  <Link to="/contato">Contato</Link>
              </li>
          </ul>
        </nav>
      </header>
    );
  }
}