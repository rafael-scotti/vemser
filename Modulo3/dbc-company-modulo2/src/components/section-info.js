import React, { Component } from 'react';
import '../css/box.css';
import '../css/buttons.css';
import LinhasDeServicoDBC_Smartsourcing from '../img/Linhas-de-Serviço-DBC_Smartsourcing.png';
import LinhasDeServiçoDBC_SoftwareBuilder from '../img/Linhas-de-Serviço-DBC_Software-Builder.png';
import LinhasDeServiçoDBC_Sustain from '../img/Linhas-de-Serviço-DBC_Sustain.png';
import LinhasDeServiçoAgil from '../img/Linhas-de-Serviço-Agil.png';
import Box from './box';

export default class SectionInfo extends Component {

  render() {
    return (
      <section className="container">
        <div className="row">
          <article className="col col-12 col-md-8 col-lg-8">
            <h2>Titulo</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, quam quaerat non quidem vitae aliquid deleniti tempora incidunt architecto temporibus expedita ex, fugit quod, ipsa rerum ipsam laboriosam repellat at?
            </p>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, quam quaerat non quidem vitae aliquid deleniti tempora incidunt architecto temporibus expedita ex, fugit quod, ipsa rerum ipsam laboriosam repellat at?
            </p>
          </article>
          <article className="col col-12 col-md-4 col-lg-4">
            <h2>Titulo</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, quam quaerat non quidem vitae aliquid deleniti tempora incidunt architecto temporibus expedita ex, fugit quod, ipsa rerum ipsam laboriosam repellat at?
            </p>
          </article>
        </div>

        <div className="row">
          <div className="col col-12 col-md-4 col-lg-3">
            <Box>
              <div>
                <img src={LinhasDeServicoDBC_Smartsourcing} alt="Nao sei" />
              </div>
              <h4>Smartsourcing</h4>
              <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem Lorem Lorem Lorem</p>
              <a className="button button-blue" href="#"> Saiba mais </a>
            </Box>
          </div>
          <div className="col col-12 col-md-4 col-lg-3">
            <Box>
              <div>
                <img src={LinhasDeServiçoDBC_SoftwareBuilder} alt="Nao sei" />
              </div>
              <h4>Software Builder</h4>
              <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem Lorem Lorem Lorem</p>
              <a className="button button-blue" href="#"> Saiba mais </a>
            </Box>
          </div>
          <div className="col col-12 col-md-4 col-lg-3">
            <Box>
              <div>
                <img src={LinhasDeServiçoDBC_Sustain} alt="Nao sei" />
              </div>
              <h4>Sustain</h4>
              <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem Lorem Lorem Lorem</p>
              <a className="button button-blue" href="#"> Saiba mais </a>
            </Box>
          </div>
          <div className="col col-12 col-md-4 col-lg-3">
            <Box>
              <div>
                <img src={LinhasDeServiçoAgil} alt="Nao sei" />
              </div>
              <h4>DBC Ágil</h4>
              <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem Lorem Lorem Lorem</p>
              <a className="button button-blue" href="#"> Saiba mais </a>
            </Box>
          </div>

        </div>
      </section >
    );
  }
}