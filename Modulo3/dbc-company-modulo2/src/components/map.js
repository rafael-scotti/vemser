import React, { Component } from 'react';
import '../css/map.css';
// import { Container } from './styles';

export default class Map extends Component {
    render() {
        return (
            <section className="mapa">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.720084476088!2d-51.170870284931276!3d-30.016192881892557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576823168092!5m2!1spt-BR!2sbr"></iframe>
            </section>
        );
    }
}
