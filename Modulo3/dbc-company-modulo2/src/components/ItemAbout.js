import React, { Component } from 'react';

export default class ItemAbout extends Component {
  render() {
    return (
      <div className="row">
        <div className="col col-12">
          <img src={this.props.img} />
          <h2> { this.props.title} </h2>
          { this.props.children }
        </div>
      </div>
        
   
    );
  }
}
