import React, { Component } from 'react';
import '../css/footer.css';
import { Link } from 'react-router-dom';
export default (props) => (
  <footer className="main-footer">
    <div className="container">
      <nav>
        <ul className="clearfix">
        <li>
                  <Link to="/">Home</Link>
              </li>
              <li>
                  <Link to="/sobre-nos">Sobre nós</Link>
              </li>
              <li>
                  <Link to="/servicos">Serviços</Link>
              </li>
              <li>
                  <Link to="/contato">Contato</Link>
              </li>
        </ul>
      </nav>
      <p>
        &copy; Copyright DBC Company - 2019
      </p>
    </div>
  </footer>
);