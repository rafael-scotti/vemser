class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.front_picture = obj.sprites.front_default;
    this.back_picture = obj.sprites.back_default;
    this.id = obj.id;
    this.name = obj.name;
    this.height = obj.height;
    this.weight = obj.weight;
    this.types = obj.types;
    this.stats = obj.stats;
  }
}
