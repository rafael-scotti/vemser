import React, { Component } from 'react';
import './App.css';

//import CompA, { CompB } from './components/ExemploComponenteBasico';
import Membros from './components/Membros';

class App extends Component {
  constructor( props ) {
    super( props );

  }

  render() {
    return (
      <div className="App">

      <Membros nome="Rafael" sobrenome="Zanella" />
      <Membros nome="Rafael" sobrenome="Scotti" />
      <Membros nome="Scotti" sobrenome="Zanella" />

    {/*     <CompA name="A"/>
        <CompA />
        <CompA />
        <CompB /> */}

      </div>
    );
  }
}


export default App;
