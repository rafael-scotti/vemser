// console.log("cheguei", "aqui");



var nomeDaVariavel = "valor";
let nomeDaLet = "valorLet"
const nomeDaConst = "valorConst"

const nomeDaConst2 = {
    nome : "Rafael",
    idade: 29
};

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Rafael Scotti"

function nomeDaFuncao() {
    let nomeDaLet = "valor2";
}

// console.log(nomeDaConst2)
// console.log(nomeDaLet);

// function somar() {

// }


function somar( valor1, valor2 = 1 ) {
    console.log( valor1 + valor2 );
}

// somar(1);
// somar(1, 3);

// console.log(1 + 1);
// console.log(1 + "1");

function ondeMoro( cidade ) {
    // console.log( "Eu moro em " + cidade + ". E sou Feliz." );
    console.log( `Eu moro em ${cidade}. E sou Feliz.` );
}

// ondeMoro("Gravataí");


function fruteira( ) {
    let texto = "Banana" 
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n";

    let newTexto = `
                Banana
                Ameixa
                Goiaba
                `;

    console.log(texto);
    console.log(newTexto);
}

// fruteira();

let eu = {
    nome : "Rafael",
    idade : 22,
    altura : 1.75
}

function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura.`)
}

// quemSou(eu)

let funcaoSomarValores = function(a, b) {
    return a + b;
}
let add = funcaoSomarValores;
let resultado = add(3, 1);

// console.log(resultado)

// DESTRUCTOR
const { nome:n, idade:i } = eu
//console.log(n, i);

const array = [1, 3, 4, 8]
const [n1, , n3, n2, n4 = 18] = array
//console.log(n1, n2, n3, n4)

function testarPessoa({ nome, idade, altura }, numero) {
    console.log(nome, idade, altura)
    console.log(numero + 1)
}

//testarPessoa(eu, 1);


// inverter elementos com destruction
let a1 = 42;
let b1 = 15;

console.log(a1, b1);

[a1, b1] = [b1, a1];

console.log(a1, b1);

