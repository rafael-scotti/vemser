import React from 'react';
import './App.css';

import ListaSeries from './model/ListaSeries';

export default class App extends React.Component{
  constructor( props ) {
    super( props );
    this.series = new ListaSeries();

      console.log("#1 - Series invalidas");
      console.log(this.series.invalidas() );

      console.log("#2 - filtro por ano (2017)");
      console.log(this.series.filtrarPorAno(2017));

      console.log("#3 - busca por nome (Steven Yeun)");
      console.log(this.series.procurarPorNome("Steven Yeun"));

      console.log("#4 - media episodios");
      console.log(this.series.mediaDeEpisodios());

      console.log("#5 - total salarios (0)");
      console.log(this.series.totalSalario(0));

      console.log("#6A - quero genero (caos)");
      console.log(this.series.queroGenero("Caos"));

      console.log("#6B - quero titulo (The)");
      console.log(this.series.queroTitulo("The"));

      console.log('#7 - creditos');
      console.log(this.series.creditos(0));

      console.log("#8 - ");
      console.log(this.series.filtrarElencoAbreviado());
  }
  
  render() {
    return (
    <div className="App">
      
    

    </div>
  );
  }
  
}

